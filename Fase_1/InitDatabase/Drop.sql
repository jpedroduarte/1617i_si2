USE AEnima
/**************************************************************************** DROP TABLE ***********************************************************************/
IF OBJECT_ID('Aluguer') IS NOT NULL
DROP TABLE Aluguer
IF OBJECT_ID('Empregado') IS NOT NULL
DROP TABLE Empregado
IF OBJECT_ID('Cliente') IS NOT NULL
DROP TABLE Cliente
IF OBJECT_ID('Pessoa') IS NOT NULL
DROP TABLE Pessoa
IF OBJECT_ID('Equipamento') IS NOT NULL
DROP TABLE Equipamento
IF OBJECT_ID('Preco') IS NOT NULL
DROP TABLE Preco
IF OBJECT_ID('Tipo') IS NOT NULL
DROP TABLE Tipo
IF OBJECT_ID('Promocao') IS NOT NULL
DROP TABLE Promocao
GO
USE AEnima

/**************************************************************************** CREATE TABLE ***********************************************************************/
CREATE TABLE Promocao
(
promocaoID				INT		IDENTITY,
dataInicio				DATE	NOT NULL,
dataFim					DATE	NOT NULL,
descontoPrecoPorCento	INT		NULL,
descontoFraccao			INT		NULL,
descricao				NVARCHAR(300),
PRIMARY KEY(promocaoID),
);

CREATE TABLE Tipo
(
nomeTipo	NVARCHAR(50),
promocaoID	INT NULL,
descricao	NVARCHAR(300) NULL,
PRIMARY KEY(nomeTipo),
CONSTRAINT FK_tipo_promocao FOREIGN KEY (promocaoID)	REFERENCES Promocao	(promocaoID)
);

CREATE TABLE Preco
(
fraccao		INT,
valor		INT NOT NULL,
validade	DATE NOT NULL,
nomeTipo	NVARCHAR(50),
PRIMARY KEY(fraccao, valor, nomeTipo),
CONSTRAINT FK_preco_tipo FOREIGN KEY (nomeTipo) REFERENCES Tipo(nomeTipo)
);

CREATE TABLE Equipamento
(
equipamentoID	INT IDENTITY,
nomeTipo		NVARCHAR(50)	NOT NULL,
descricao		NVARCHAR(300)	NULL,
PRIMARY KEY (equipamentoID),
CONSTRAINT FK_equipamento_tipo FOREIGN KEY (nomeTipo) REFERENCES Tipo(nomeTipo)
);

CREATE TABLE Pessoa
(
pessoaID	INT				IDENTITY,
nome		NVARCHAR(100)	NULL,
PRIMARY KEY (pessoaID)
);

CREATE TABLE Cliente
(
codigoCliente	INT IDENTITY,
pessoaID		INT NOT NULL,
nif				INT NULL UNIQUE,
morada			NVARCHAR(300) NULL,
PRIMARY KEY(codigoCliente),
CONSTRAINT FK_cliente_pessoa FOREIGN KEY (pessoaID) REFERENCES Pessoa(pessoaID) ON DELETE CASCADE
);

CREATE TABLE Empregado
(
codigoEmpregado		INT IDENTITY,
pessoaID			INT NOT NULL,
PRIMARY KEY(codigoEmpregado),
CONSTRAINT FK_empregado_pessoa FOREIGN KEY (pessoaID) REFERENCES Pessoa(pessoaID) ON DELETE CASCADE
);

CREATE TABLE Aluguer
(
numeroSerie		INT				IDENTITY,
equipamentoID	INT				NOT NULL,
codigoCliente	INT				NOT NULL, 
codigoEmpregado INT				NOT NULL,
duracao			INT				NULL,
dataHora		DATETIME		NOT NULL DEFAULT GETDATE(),
dataFim			DATETIME		NULL,
valor			DECIMAL(6, 2)	NULL,
PRIMARY KEY(numeroSerie),
CONSTRAINT FK_aluguer_equipamento	FOREIGN KEY (equipamentoID)		REFERENCES Equipamento	(equipamentoID) ON DELETE CASCADE,
CONSTRAINT FK_aluguer_cliente		FOREIGN KEY (codigoCliente)		REFERENCES Cliente		(codigoCliente) ON DELETE CASCADE,
CONSTRAINT FK_aluguer_empregado		FOREIGN KEY (codigoEmpregado)	REFERENCES Empregado	(codigoEmpregado)
);
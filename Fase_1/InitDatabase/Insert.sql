USE AEnima

/**************************************************************************** INSERT INTO TABLE ***********************************************************************/

INSERT INTO dbo.Promocao (dataInicio, dataFim, descontoPrecoPorCento, descontoFraccao, descricao) VALUES 
						 ('2016-01-01', '2016-12-31', 5, NULL, 'Desconto de 5% todo o ano de 2016'), 
						 ('2016-06-01', '2016-08-31', NULL, 30, 'Desconto de 30 minutos na frac��o durante o Ver�o de 2016'),
						 ('2016-05-01', '2016-05-31', 20, NULL, 'Desconto de 20% no m�s de Maio');

INSERT INTO dbo.Tipo	(nomeTipo, promocaoID, descricao) VALUES 
						 ('Canoa', 1, 'Canoa'), 
						 ('Gaivota', 2, 'Gaivota'),
						 ('Toldo', 3, 'Toldo'),
						 ('Barco', NULL, 'Barco n�o tem promo��es');

declare @exampleDate date = DATEADD(DAY, 30, GETDATE())

INSERT INTO dbo.Preco	(fraccao, valor, validade, nomeTipo) VALUES 
						 (30, 20, @exampleDate, 'Canoa'),
						 (60, 35, @exampleDate, 'Canoa'),
						 (90, 50, @exampleDate, 'Canoa'),
						 (8*60, 120, @exampleDate, 'Canoa'),
						 (30, 25, @exampleDate, 'Gaivota'),
						 (60, 45, @exampleDate, 'Gaivota'),
						 (90, 60, @exampleDate, 'Gaivota'),
						 (8*60, 135, @exampleDate, 'Gaivota'),
						 (2*60, 20, @exampleDate, 'Toldo'),
						 (4*60, 35, @exampleDate, 'Toldo'),
						 (6*60, 50, @exampleDate, 'Toldo'),
						 (8*60, 80, @exampleDate, 'Toldo'),
						 (30, 60, @exampleDate, 'Barco'),
						 (60, 100, @exampleDate, 'Barco'),
						 (90, 125, @exampleDate, 'Barco'),
						 (4*60, 250, @exampleDate, 'Barco'),
						 (8*60, 500, @exampleDate, 'Barco');

INSERT INTO dbo.Equipamento (nomeTipo, descricao) VALUES
						 ('Canoa', 'Canoa n�1'),
						 ('Canoa', 'Canoa n�2'),
						 ('Canoa', 'Canoa n�3'),
						 ('Canoa', 'Canoa n�4'),
						 ('Canoa', 'Canoa n�5'),
						 ('Canoa', 'Canoa n�6'),
						 ('Gaivota', 'Gaivota n�1'),
						 ('Gaivota', 'Gaivota n�2'),
						 ('Gaivota', 'Gaivota n�3'),
						 ('Gaivota', 'Gaivota n�4'),
						 ('Gaivota', 'Gaivota n�5'),
						 ('Gaivota', 'Gaivota n�6'),
						 ('Toldo', 'Toldo n�1'),
						 ('Toldo', 'Toldo n�2'),
						 ('Toldo', 'Toldo n�3'),
						 ('Toldo', 'Toldo n�4'),
						 ('Toldo', 'Toldo n�5'),
						 ('Toldo', 'Toldo n�6'),
						 ('Toldo', 'Toldo n�7'),
						 ('Toldo', 'Toldo n�8'),
						 ('Toldo', 'Toldo n�9'),
						 ('Toldo', 'Toldo n�10'),
						 ('Barco', 'Barco n�1'),
						 ('Barco', 'Barco n�2'),
						 ('Barco', 'Barco n�3');

INSERT INTO dbo.Pessoa (nome) VALUES
						 ('Consumidor Final'),
						 ('Rafael Macedo'),
						 ('Andr� Carona'),
						 ('Jo�o Silva'),
						 ('Luis Pinto'),
						 ('F�bio Martin'),
						 ('Bruno Louren�o'),
						 ('Raimunda Frigida'),
						 ('Joana Castanheira'),
						 ('Jo�o Castanho'),
						 ('Vladimiro Putin'),
						 ('Carlos Lopes'),
						 ('Helena Marques'),
						 ('Barack Obama'),
						 ('Ana Luz'),
						 ('Duarte Teixeira'),
						 ('Ana Rita Pereira'),
						 ('Francisco Lopes'),
						 ('Tiago Louren�o'),
						 ('Vasco Peres'),
						 ('J�lia Pereira');

INSERT INTO dbo.Empregado (pessoaID) VALUES
						 (2),
						 (6),
						 (15),
						 (19),
						 (3);

INSERT INTO dbo.Cliente (pessoaID, nif, morada) VALUES
						(1, 999999999, 'Sem morada'),
						(2, 236985478, 'Avenida da Liberdade'),
						(4, 125896574 ,'Avenida Marechal Gomes da Costa'),
						(5, 365887456, 'Avenida Jo�o XXI'),
						(7, 125774562, 'Rua da Bela Vista a Gra�a'),
						(8, 123654789, 'Praceta Rog�rio Paulo'),
						(9, 125863257, 'Praceta Irene Lisboa'),
						(10, 255644856, 'Avenida do Ocean�rio'),
						(11, 254621456, 'Avenida Bocage'),
						(12, 125874523, 'Avenida das T�lipas'),
						(13, 125447852, 'Avenida 24 de Julho'),
						(14, 123669554, 'Pra�a do Municipio'),
						(16, 236566587, 'Avenida dos Bombeiros Volunt�rios'),
						(17, 231456987, 'Rua Serpa Pinto'),
						(18, 125478541, 'Rua do Sapateiro'),
						(20, 121114521, 'Rua do Mercado de Campo de Ourique');

INSERT INTO dbo.Aluguer (equipamentoID, codigoCliente, codigoEmpregado) VALUES
						(23, 5, 1),
						(5, 5, 2);
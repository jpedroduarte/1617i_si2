use AEnima

IF OBJECT_ID('view_equipamento_within_last_week') IS NOT NULL
DROP VIEW view_equipamento_within_last_week;

GO

/**
* Al�nea k
*/

create view view_equipamento_within_last_week as
select dbo.Aluguer.*
from dbo.Aluguer INNER JOIN dbo.Equipamento on (dbo.Aluguer.equipamentoID = dbo.Equipamento.equipamentoID)
where dataHora < DATEADD(day,-7, GETDATE())

/**
* Testes
*/

GO

select * from dbo.Aluguer
select * from view_equipamento_within_last_week
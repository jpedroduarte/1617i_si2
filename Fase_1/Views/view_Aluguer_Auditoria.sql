USE AEnima;
GO
IF OBJECT_ID('v_Aluguer_Auditoria') IS NOT NULL
DROP VIEW dbo.v_Aluguer_Auditoria
GO
CREATE VIEW v_Aluguer_Auditoria AS
SELECT [numeroSerie]
      ,[equipamentoID]
      ,[codigoCliente]
      ,[codigoEmpregado]
      ,[duracao]
      ,[dataHora]
      ,[dataFim]
      ,[valor]
  FROM [dbo].[Aluguer]
UNION
SELECT [numeroSerie]
      ,[equipamentoID]
      ,[codigoCliente]
      ,[codigoEmpregado]
      ,[duracao]
      ,[dataHora]
      ,[dataFim]
      ,[valor]
  FROM [dbo].[Historico]
 
/**
* Tests
*/
GO
DELETE FROM dbo.Aluguer
WHERE numeroSerie= 1;

select * from dbo.Aluguer
select * from dbo.Historico

select * from dbo.v_Aluguer_Auditoria

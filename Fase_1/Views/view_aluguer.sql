/**************************************************************************** VIEWS ***********************************************************************/
/*
Esta vista mostra os alugueres sem qualquer tipo de filtro
NOTA: o campo 'Dura��o Total' consiste na diferen�a da data
inicial menos a data final, truncadas.
*/
USE AEnima
IF OBJECT_ID('v_Aluguer') IS NOT NULL
DROP VIEW v_Aluguer;

GO

CREATE VIEW v_Aluguer AS
SELECT 
	Cliente.nome AS 'Nome de Cliente', 
	C.nif AS 'NIF', 
	C.morada AS 'Morada', 
	A.numeroSerie AS 'N�mero de S�rie',
	Eq.nomeTipo AS 'Equipamento',
	Promo.descontoFraccao AS 'Desconto Temporal',
	Promo.descontoPrecoPorCento AS 'Desconto %',
	(SELECT DATEDIFF(MINUTE,( A.dataHora), (A.dataFim))) AS 'Duracao Total', 
	A.dataHora AS 'Data de Inicio', 
	A.dataFim AS 'Data de Fim',
	A.valor AS 'Preco Final',
	Empregado.nome AS 'Nome do Empregado'
FROM 
	dbo.Aluguer AS A
	INNER JOIN
	dbo.Cliente AS C
		ON(A.codigoCliente = C.pessoaID)
		INNER JOIN
		dbo.Pessoa Cliente
			ON(C.pessoaID = Cliente.pessoaID)
	INNER JOIN
	dbo.Empregado AS E
		ON(A.codigoEmpregado = E.pessoaID)
		INNER JOIN
		dbo.Pessoa Empregado
			ON(E.pessoaID = Empregado.pessoaID)
	INNER JOIN
	dbo.Equipamento AS Eq
		ON(A.equipamentoID = Eq.equipamentoID)
		INNER JOIN
		dbo.Tipo AS T
			ON(Eq.nomeTipo = T.nomeTipo)
			LEFT JOIN
			dbo.Promocao AS Promo
				ON(T.promocaoID = Promo.promocaoID)
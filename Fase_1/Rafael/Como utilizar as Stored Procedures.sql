USE AEnima
GO
/**************************************************************CLIENTES**************************************************************/

/*******************Como introduzir um Cliente*****************************************/
EXEC dbo.sp_insertCliente 'Rodrigo Figueirinha', 125896547, 'Rua das Figuiras, 25'

/*******************Como alterar o nome de um Cliente**********************************/
DECLARE @idPessoa INT
/*Primeiro tem que se saber qual o pessoaID associado ao Cliente, procurando pelo Nome, ou pelo NIF, ou pelo dois*/
EXEC dbo.sp_getPessoaID 'Rodrigo Figueirinha', NULL, @idPessoa OUTPUT
/*Fazer a pesquisa do Cliente pelo seu pessoaID e alterar o nome*/
EXEC dbo.sp_updateNomeCliente @idPessoa, 'Rodrigo Figueiredo'
GO
/*******************Como alterar o NIF de um Cliente***********************************/
DECLARE @idPessoa INT
/*Primeiro tem que se saber qual o pessoaID associado ao Cliente, procurando pelo NIF, ou pelo Nome, ou pelos dois*/
EXEC dbo.sp_getPessoaID NULL, 125896547, @idPessoa OUTPUT
/*Fazer a pesquisa do Cliente pelo seu pessoaID e alterar o NIF*/
EXEC dbo.sp_updateNifCliente @idPessoa, 236394590
GO
/*******************Como alterar a Morada de um Cliente********************************/
DECLARE @idPessoa INT
/*Primeiro tem que se saber qual o pessoaID associado ao Cliente, procurando pelos dois, ou s� pelo Nome, ou s� pelo NIF*/
EXEC dbo.sp_getPessoaID 'Rodrigo Figueiredo', 236394590, @idPessoa OUTPUT
/*Fazer a pesquisa do Cliente pelo seu pessoaID e alterar a Morada*/
EXEC dbo.sp_updateMoradaCliente @idPessoa, 'Morada teste'
GO
/*******************Como remover um Cliente********************************************/
DECLARE @idPessoa INT
/*Primeiro tem que se saber qual o pessoaID associado ao Cliente, procurando pelos dois, ou s� pelo Nome, ou s� pelo NIF*/
EXEC dbo.sp_getPessoaID NULL, 236394590, @idPessoa OUTPUT
/*Fazer a pesquisa do Cliente pelo seu pessoaID e apag�-lo*/
EXEC dbo.sp_deleteCliente @idPessoa
GO

/**************************************************************EMPREGADOS**************************************************************/

/*******************Como introduzir um Empregado***************************************/
EXEC dbo.sp_insertEmpregado 'Andreia Assun��o'
GO

/*************************************************************EQUIPAMENTOS*************************************************************/

/*********************Como introduzir um Equipamento***********************************/
/*
Default � uma FLAG que aponta para o campo "disponibilidade" do tipo BIT que permite atribuir a um aluguer, 
desde que esteja a 1, sen�o est� a 0 e quer dizer que est� a ser usada neste momento
*/
EXEC dbo.sp_insertEquipamento 'Canoa', 'Canoa inserida com Stored Procedure', DEFAULT
GO

/*******************Como alterar um Equipamento****************************************/
EXEC dbo.sp_updateEquipamento 23, 'Toldo', 'Equipamento alterado com Stored Procedure', 0
GO

/*******************Como alterar a disponibilidade de um Equipamento*******************/
/*
Este Procedimento serve para meter o equipamento indisponivel, para o caso de estar em manuten��o, por exemplo.
Para colocar um equipamento indisponivel quando se faz um aluguer, n�o precisa de chamar este procedimento porque j�
est� a ser realizado com o Trigger 'tr_equipamentoIndisponivel', que colocao indisponivel o equipamento em quest�o.
*/
EXEC dbo.sp_updateDisponibilidadeEquipamento 12, 1
GO

/*******************Como remover um Equipamento***************************************/
EXEC dbo.sp_deleteEquipamento 20
GO

/***************************************************************ALUGUER***************************************************************/

/*******************Como inserir um Aluguer*******************************************/
/*Cliente que n�o existe*/
EXEC dbo.sp_insertCliente 'Joana MLM', 255030991, 'Rua da Bela Vista a Gra�a, 31'
DECLARE @clienteID INT
EXEC dbo.sp_getPessoaID NULL, 255030991, @clienteID OUTPUT
PRINT CONCAT('Cliente @clienteID: ',@clienteID)
DECLARE @empregadoID INT
EXEC dbo.sp_insertEmpregado 'Jos� Dias'
EXEC dbo.sp_getPessoaID 'Jos� Dias', NULL, @empregadoID OUTPUT
PRINT CONCAT('Empregado @empregadoID: ',@empregadoID)
EXEC dbo.sp_aluguer_clienteRegistado @clienteID, 20, @empregadoID

/*Cliente que j� existe*/
EXEC dbo.sp_aluguer_clienteRegistado 10, 19, 3
GO
/*Entregar o Equipamento e fechar o aluguer*/
EXEC dbo.sp_entrega_aluguer 1
GO
/*Apagar um Alguer da Base de dados*/
EXEC dbo.sp_deleteAluguer 20
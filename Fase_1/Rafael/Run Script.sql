USE AEnima
/**************************************************************************** DROP TABLE ***********************************************************************/
IF OBJECT_ID('Aluguer') IS NOT NULL
DROP TABLE Aluguer
IF OBJECT_ID('Empregado') IS NOT NULL
DROP TABLE Empregado
IF OBJECT_ID('Cliente') IS NOT NULL
DROP TABLE Cliente
IF OBJECT_ID('Pessoa') IS NOT NULL
DROP TABLE Pessoa
IF OBJECT_ID('Equipamento') IS NOT NULL
DROP TABLE Equipamento
IF OBJECT_ID('Preco') IS NOT NULL
DROP TABLE Preco
IF OBJECT_ID('Tipo') IS NOT NULL
DROP TABLE Tipo
IF OBJECT_ID('Promocao') IS NOT NULL
DROP TABLE Promocao
GO
IF OBJECT_ID('Historico') IS NOT NULL
DROP TABLE Historico
GO
/**************************************************************************** CREATE TABLE ***********************************************************************/
CREATE TABLE Promocao
(
promocaoID				INT		IDENTITY,
dataInicio				DATE	NOT NULL,
dataFim					DATE	NOT NULL,
descontoPrecoPorCento	INT		NULL,
descontoFraccao			INT		NULL,
descricao				NVARCHAR(300),
PRIMARY KEY(promocaoID),
);

CREATE TABLE Tipo
(
nomeTipo	NVARCHAR(50),
promocaoID	INT NULL,
descricao	NVARCHAR(300) NULL,
PRIMARY KEY(nomeTipo),
CONSTRAINT FK_tipo_promocao FOREIGN KEY (promocaoID)	REFERENCES Promocao	(promocaoID)
);

CREATE TABLE Preco
(
fraccao		INT,
valor		INT NOT NULL,
nomeTipo	NVARCHAR(50),
validade	DATE NOT NULL,
PRIMARY KEY(fraccao, nomeTipo),
CONSTRAINT FK_preco_tipo FOREIGN KEY (nomeTipo) REFERENCES Tipo(nomeTipo)
);

CREATE TABLE Equipamento
(
equipamentoID	INT IDENTITY,
nomeTipo		NVARCHAR(50)	NOT NULL,
descricao		NVARCHAR(300)	NULL,
disponibilidade	BIT				NOT NULL,
PRIMARY KEY (equipamentoID),
CONSTRAINT FK_equipamento_tipo FOREIGN KEY (nomeTipo) REFERENCES Tipo(nomeTipo)
);

CREATE TABLE Pessoa
(
pessoaID	INT				IDENTITY,
nome		NVARCHAR(100)	NULL,
PRIMARY KEY (pessoaID)
);

CREATE TABLE Cliente
(
pessoaID		INT NOT NULL,
nif				INT NULL /*UNIQUE*/,
morada			NVARCHAR(300) NULL,
PRIMARY KEY(pessoaID),
CONSTRAINT FK_cliente_pessoa FOREIGN KEY (pessoaID) REFERENCES Pessoa(pessoaID) ON DELETE CASCADE
);

CREATE TABLE Empregado
(
pessoaID			INT NOT NULL,
PRIMARY KEY(pessoaID),
CONSTRAINT FK_empregado_pessoa FOREIGN KEY (pessoaID) REFERENCES Pessoa(pessoaID) ON DELETE CASCADE
);

CREATE TABLE Aluguer
(
numeroSerie		INT				IDENTITY,
equipamentoID	INT				NOT NULL,
codigoCliente	INT				NOT NULL, 
codigoEmpregado INT				NOT NULL,
duracao			INT				NULL,
dataHora		DATETIME		NOT NULL DEFAULT GETDATE(),
dataFim			DATETIME		NULL,
valor			DECIMAL(6, 2)	NULL,
PRIMARY KEY(numeroSerie),
CONSTRAINT FK_aluguer_equipamento	FOREIGN KEY (equipamentoID)		REFERENCES Equipamento	(equipamentoID) ON DELETE CASCADE,
CONSTRAINT FK_aluguer_cliente		FOREIGN KEY (codigoCliente)		REFERENCES Cliente		(pessoaID) ON DELETE CASCADE,
CONSTRAINT FK_aluguer_empregado		FOREIGN KEY (codigoEmpregado)	REFERENCES Empregado	(pessoaID)
);

CREATE TABLE Historico
(
historicoID		INT				IDENTITY,
numeroSerie		INT				NOT NULL,
equipamentoID	INT				NOT NULL,
codigoCliente	INT				NOT NULL, 
codigoEmpregado INT				NOT NULL,
duracao			INT				NULL,
dataHora		DATETIME		NOT NULL,
dataFim			DATETIME		NULL,
valor			DECIMAL(6, 2)	NULL,
PRIMARY KEY(historicoID)
);

/**************************************************************************** FUNCTIONS ***********************************************************************/
/*
Esta funcao mostra os equipamentos dispon�veis de um determinado tipo e dentro de um determinado per�odo
Optou-se por usar uma function, em vez de view ou stored procedure, porque s� precisamos de listar e passar
como parametros o tipo e periodo
*/
IF OBJECT_ID('equip_livres_tempo_tipo') IS NOT NULL
DROP FUNCTION dbo.equip_livres_tempo_tipo
GO
CREATE FUNCTION dbo.equip_livres_tempo_tipo
(
@dataInicio AS DATE,
@dataFim AS DATE,
@tipo AS NVARCHAR(50)
)
RETURNS TABLE
	RETURN
	(
		SELECT Equipamento.*
		FROM Equipamento
		WHERE equipamentoID NOT IN (
			SELECT A.equipamentoID
			FROM dbo.Aluguer AS A
			INNER JOIN dbo.Equipamento AS E
			ON (A.equipamentoID = E.equipamentoID)
			WHERE (A.dataHora BETWEEN @dataInicio AND @dataFim OR A.dataFim IS NULL)
		)
		AND nomeTipo = @tipo
	)
;
GO

/*
Function - Get price from closest timestamp in Prices table
*/
IF OBJECT_ID('get_price_from_aluguer_time') IS NOT NULL
DROP FUNCTION get_price_from_aluguer_time
GO
CREATE FUNCTION dbo.get_price_from_aluguer_time
(
@tipo AS NVARCHAR(MAX),
@time AS INT
)
RETURNS TABLE
AS RETURN 
	(
		SELECT TOP 1 P.valor, P.fraccao FROM dbo.Preco AS P
		WHERE nomeTipo = @tipo AND fraccao >= @time AND validade > GETDATE()
		ORDER BY ABS(fraccao - @time)
	)
GO

/**************************************************************************** TRIGGERS ***********************************************************************/
/*
Este trigger faz insert na Tabela Historico quando se remove um tuplo da tabela Aluguer
*/
IF OBJECT_ID('tr_removeAluguer') IS NOT NULL
DROP TRIGGER tr_removeAluguer;
GO
CREATE TRIGGER tr_removeAluguer
ON dbo.Aluguer
AFTER DELETE
AS BEGIN
	DECLARE @numeroSerie INT, @equipamentoID INT, @codigoCliente INT, @codigoEmpregado INT, @duracao INT, @dataHora DATE, @dataFim DATE, @valor DECIMAL(6,2)
	SELECT @numeroSerie = numeroSerie, @equipamentoID = equipamentoID, @codigoCliente = codigoCliente, @codigoEmpregado = codigoEmpregado, @duracao = duracao, @dataHora = dataHora, @dataFim = dataFim, @valor = valor FROM deleted;
	INSERT INTO dbo.Historico (numeroSerie, equipamentoID, codigoCliente, codigoEmpregado, duracao, dataHora, dataFim, valor) VALUES (@numeroSerie, @equipamentoID, @codigoCliente, @codigoEmpregado, @duracao, @dataHora, @dataFim, @valor);
END
GO

/*
Este trigger faz update na Tabela Equipamento, colocando o Equipamento que est� a ser alugado como
indisponivel
*/
IF OBJECT_ID('tr_insertAluguer') IS NOT NULL
DROP TRIGGER tr_insertAluguer;
GO
CREATE TRIGGER tr_insertAluguer
ON dbo.Aluguer
AFTER INSERT
AS BEGIN
	SET NOCOUNT ON;
	/*Pode-se chamar o Procedimento 'sp_updateDisponibilidadeEquipamento'*/
	UPDATE dbo.Equipamento SET disponibilidade = 0 WHERE equipamentoID = (SELECT equipamentoID FROM inserted)
END
GO
/*
Este trigger faz update na propria tabela, apenas quando o campo 'dataFim' � alterado,
alterando o campo 'duracao' que � a diferen�a entre a data de inicio e a data de fim
em minutos.
*/
IF OBJECT_ID('tr_updateValor') IS NOT NULL
DROP TRIGGER tr_updateValor;
GO
CREATE TRIGGER tr_updateValor
ON dbo.Aluguer
AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @desc AS BIT
	IF UPDATE(dataFim)
	/*Actualiza a dura��o na tabela Aluguer, fazendo a diferen�a entre a data final de Aluguer menos a data inicial, convertendo para minutos*/
	BEGIN
		UPDATE Aluguer 
		SET duracao = (SELECT DATEDIFF(MINUTE, (SELECT dataHora FROM inserted), (SELECT dataFim FROM inserted)))
		WHERE numeroSerie = (SELECT numeroSerie FROM inserted);
	END
	/*Cada vez que actualizamos o campo dataFim da tabela Aluguer, estamos no fundo a entregar o equipamento de volta, fazendo sentido colocar aquele equipamento em especifico disponivel para voltar a ser alugado*/
	BEGIN
		UPDATE dbo.Equipamento
		SET disponibilidade = 1
		WHERE equipamentoID = (SELECT equipamentoID FROM inserted)
	END
	/*Quando actualizamos o campo dataFim, temos de calcular o pre�o do Aluguer, tendo em conta as Promo��es que aquele tipo de Equipamento t�m*/
	BEGIN
		DECLARE @descontoPorCento INT, @descontoFraccao INT, @valorFinal DECIMAL(6, 2), @numeroSerie INT, @duracaoInicial INT, @fraccaoFinal INT, @precoFinal DECIMAL(6, 2), @nomeTipo NVARCHAR(50);
		SELECT @numeroSerie = numeroSerie FROM inserted;
		
		/*Procura se h� Promo��es*/
		SELECT @descontoPorCento = Promo.descontoPrecoPorCento, @descontoFraccao = Promo.descontoFraccao, @duracaoInicial = A.duracao
		FROM dbo.Aluguer AS A JOIN dbo.Equipamento AS E ON(A.equipamentoID = E.equipamentoID)
			JOIN dbo.Tipo AS T ON(E.nomeTipo = T.nomeTipo)
				JOIN dbo.Promocao AS Promo ON(T.promocaoID = Promo.promocaoID)
		WHERE A.equipamentoID = (SELECT equipamentoID FROM inserted)

		/*Tr�s de volta o nome do Equipamento*/
		SELECT @nomeTipo = E.nomeTipo FROM dbo.Aluguer AS A JOIN dbo.Equipamento AS E ON(A.equipamentoID = E.equipamentoID) WHERE A.equipamentoID = (SELECT equipamentoID FROM inserted);
		
		IF (@descontoFraccao IS NOT NULL)
			BEGIN
				SELECT @fraccaoFinal = fraccao, @precoFinal = valor FROM dbo.get_price_from_aluguer_time(@nomeTipo, (@duracaoInicial-@descontoFraccao));
			END
		ELSE IF (@descontoPorCento IS NOT NULL)
			BEGIN
				SELECT @precoFinal = (valor - ((valor*@descontoPorCento)/100)) FROM dbo.get_price_from_aluguer_time(@nomeTipo, @duracaoInicial);
			END
		ELSE
			BEGIN
				SELECT @precoFinal = valor FROM dbo.get_price_from_aluguer_time(@nomeTipo, @duracaoInicial);
			END
		
		BEGIN TRAN
			UPDATE dbo.Aluguer SET valor = @precoFinal WHERE numeroSerie = @numeroSerie
		COMMIT TRAN
	END
END
GO
/**************************************************************************** VIEWS ***********************************************************************/
/*
Esta vista mostra os alugueres sem qualquer tipo de filtro
NOTA: o campo 'Dura��o Total' consiste na diferen�a da data
inicial menos a data final, truncadas.
*/
IF OBJECT_ID('v_Aluguer') IS NOT NULL
DROP VIEW v_Aluguer;
GO
CREATE VIEW v_Aluguer AS
SELECT 
	Cliente.nome AS 'Nome de Cliente', 
	C.nif AS 'NIF', 
	C.morada AS 'Morada', 
	A.numeroSerie AS 'N�mero de S�rie',
	Eq.nomeTipo AS 'Equipamento',
	Promo.descontoFraccao AS 'Desconto Temporal',
	Promo.descontoPrecoPorCento AS 'Desconto %',
	A.duracao AS 'Duracao Total',
	A.dataHora AS 'Data de Inicio', 
	A.dataFim AS 'Data de Fim',
	A.valor AS 'Valor a Pagar',
	Empregado.nome AS 'Nome do Empregado'
FROM 
	dbo.Aluguer AS A
	INNER JOIN
	dbo.Cliente AS C
		ON(A.codigoCliente = C.pessoaID)
		INNER JOIN
		dbo.Pessoa Cliente
			ON(C.pessoaID = Cliente.pessoaID)
	INNER JOIN
	dbo.Empregado AS E
		ON(A.codigoEmpregado = E.pessoaID)
		INNER JOIN
		dbo.Pessoa Empregado
			ON(E.pessoaID = Empregado.pessoaID)
	INNER JOIN
	dbo.Equipamento AS Eq
		ON(A.equipamentoID = Eq.equipamentoID)
		INNER JOIN
		dbo.Tipo AS T
			ON(Eq.nomeTipo = T.nomeTipo)
			LEFT JOIN
			dbo.Promocao AS Promo
				ON(T.promocaoID = Promo.promocaoID)
GO

/*
Esta vista mostra os alugueres que ainda est�o activos sem qualquer tipo de filtro
NOTA: o campo 'Dura��o Total' consiste na diferen�a da data
inicial menos a data final, truncadas.
*/
IF OBJECT_ID('v_AluguerActivo') IS NOT NULL
DROP VIEW v_AluguerActivo;
GO
CREATE VIEW v_AluguerActivo AS
SELECT 
	Cliente.nome AS 'Nome de Cliente', 
	C.nif AS 'NIF', 
	C.morada AS 'Morada', 
	A.numeroSerie AS 'N�mero de S�rie',
	Eq.nomeTipo AS 'Equipamento',
	Promo.descontoFraccao AS 'Desconto Temporal',
	Promo.descontoPrecoPorCento AS 'Desconto %',
	A.duracao AS 'Duracao Total',
	A.dataHora AS 'Data de Inicio', 
	A.dataFim AS 'Data de Fim',
	A.valor AS 'Valor a Pagar',
	Empregado.nome AS 'Nome do Empregado'
FROM 
	dbo.Aluguer AS A
	INNER JOIN
	dbo.Cliente AS C
		ON(A.codigoCliente = C.pessoaID)
		INNER JOIN
		dbo.Pessoa Cliente
			ON(C.pessoaID = Cliente.pessoaID)
	INNER JOIN
	dbo.Empregado AS E
		ON(A.codigoEmpregado = E.pessoaID)
		INNER JOIN
		dbo.Pessoa Empregado
			ON(E.pessoaID = Empregado.pessoaID)
	INNER JOIN
	dbo.Equipamento AS Eq
		ON(A.equipamentoID = Eq.equipamentoID)
		INNER JOIN
		dbo.Tipo AS T
			ON(Eq.nomeTipo = T.nomeTipo)
			LEFT JOIN
			dbo.Promocao AS Promo
				ON(T.promocaoID = Promo.promocaoID)
	WHERE A.dataFim IS NULL
GO

/*
Esta vista mostra os alugueres que j� n�o est�o activos sem qualquer tipo de filtro
NOTA: o campo 'Dura��o Total' consiste na diferen�a da data
inicial menos a data final, truncadas.
*/
IF OBJECT_ID('v_AluguerInactivo') IS NOT NULL
DROP VIEW v_AluguerInactivo;
GO
CREATE VIEW v_AluguerInactivo AS
SELECT 
	Cliente.nome AS 'Nome de Cliente', 
	C.nif AS 'NIF', 
	C.morada AS 'Morada', 
	A.numeroSerie AS 'N�mero de S�rie',
	Eq.nomeTipo AS 'Equipamento',
	Promo.descontoFraccao AS 'Desconto Temporal',
	Promo.descontoPrecoPorCento AS 'Desconto %',
	A.duracao AS 'Duracao Total',
	A.dataHora AS 'Data de Inicio', 
	A.dataFim AS 'Data de Fim',
	A.valor AS 'Valor a Pagar',
	Empregado.nome AS 'Nome do Empregado'
FROM 
	dbo.Aluguer AS A
	INNER JOIN
	dbo.Cliente AS C
		ON(A.codigoCliente = C.pessoaID)
		INNER JOIN
		dbo.Pessoa Cliente
			ON(C.pessoaID = Cliente.pessoaID)
	INNER JOIN
	dbo.Empregado AS E
		ON(A.codigoEmpregado = E.pessoaID)
		INNER JOIN
		dbo.Pessoa Empregado
			ON(E.pessoaID = Empregado.pessoaID)
	INNER JOIN
	dbo.Equipamento AS Eq
		ON(A.equipamentoID = Eq.equipamentoID)
		INNER JOIN
		dbo.Tipo AS T
			ON(Eq.nomeTipo = T.nomeTipo)
			LEFT JOIN
			dbo.Promocao AS Promo
				ON(T.promocaoID = Promo.promocaoID)
	WHERE A.dataFim IS NOT NULL
GO

/**
* Al�nea k
*/
IF OBJECT_ID('view_equipamento_within_last_week') IS NOT NULL
DROP VIEW view_equipamento_within_last_week;
GO
CREATE VIEW view_equipamento_within_last_week
AS
--SELECT dbo.Aluguer.*
--FROM dbo.Aluguer INNER JOIN dbo.Equipamento ON (dbo.Aluguer.equipamentoID = dbo.Equipamento.equipamentoID)
--WHERE dataHora < DATEADD(day,-7, GETDATE())
SELECT A.numeroSerie,
	   E.descricao,
	   C.Nome AS cliente,
	   EMP.Nome AS empregrado,
	   A.dataHora AS inicio,
	   A.dataFim AS fim,
	   A.duracao,
	   A.valor
FROM dbo.Aluguer AS A
INNER JOIN dbo.Equipamento AS E
ON (A.equipamentoID = E.equipamentoID)
INNER JOIN dbo.Pessoa AS C
ON (A.codigoCliente = C.pessoaID)
INNER JOIN dbo.Pessoa AS EMP
ON (A.codigoEmpregado = EMP.pessoaID)
WHERE dataHora < DATEADD(day,-7, GETDATE())
GO

/**
* Serve para listar todos os alugueres sejam eles removidos ou n�o, para efeitos de auditoria.
*/
IF OBJECT_ID('v_Aluguer_Auditoria') IS NOT NULL
DROP VIEW dbo.v_Aluguer_Auditoria
GO
CREATE VIEW v_Aluguer_Auditoria AS
SELECT [numeroSerie]
      ,[equipamentoID]
      ,[codigoCliente]
      ,[codigoEmpregado]
      ,[duracao]
      ,[dataHora]
      ,[dataFim]
      ,[valor]
  FROM [dbo].[Aluguer]
UNION
SELECT [numeroSerie]
      ,[equipamentoID]
      ,[codigoCliente]
      ,[codigoEmpregado]
      ,[duracao]
      ,[dataHora]
      ,[dataFim]
      ,[valor]
  FROM [dbo].[Historico]
GO
/**************************************************************************** STORED PROCEDURES ***********************************************************************/
/*********************************CLIENTE*******************************************/
/*
Este procedimento armazenado serve para criar um aluguer
quando n�o existe um cliente criado, podendo-se criar na
mesma altura
*/
IF OBJECT_ID('sp_aluguer_clienteRegistado') IS NOT NULL
DROP PROCEDURE sp_aluguer_clienteRegistado;
GO
CREATE PROCEDURE sp_aluguer_clienteRegistado
@clienteID INT,
@equipamentoID INT,
@codigoEmpregado INT
AS BEGIN
	DECLARE @disponibilidade INT
	DECLARE @cliente INT
	DECLARE @empregado INT

	SELECT @disponibilidade = disponibilidade FROM dbo.Equipamento WHERE equipamentoID = @equipamentoID
	SELECT @cliente = pessoaID FROM dbo.Cliente WHERE pessoaID = @clienteID
	SELECT @empregado = pessoaID FROM dbo.Empregado WHERE pessoaID = @codigoEmpregado
	IF (@disponibilidade = 0)
		RAISERROR('O equipamento que escolheu n�o est� disponivel!',16,1)
	IF (@cliente IS NULL)
		RAISERROR('O cliente que escolheu n�o existe!',16,1)
	IF (@empregado IS NULL)
		RAISERROR('O empregado que escolheu n�o existe!',16,1)
	ELSE
		BEGIN
			INSERT INTO dbo.Aluguer (equipamentoID, codigoCliente, codigoEmpregado) VALUES (@equipamentoID, @clienteID, @codigoEmpregado)
		END
END
GO

/*
Este procedimento armazenado serve para criar um aluguer
quando n�o existe um cliente criado, podendo-se criar na
mesma altura
*/
IF OBJECT_ID('sp_aluguer_clienteNaoRegistado') IS NOT NULL
DROP PROCEDURE sp_aluguer_clienteNaoRegistado;
GO
CREATE PROCEDURE sp_aluguer_clienteNaoRegistado
@name NVARCHAR(100),
@nif INT,
@morada NVARCHAR(300),
@equipamentoID INT,
@codigoEmpregado INT
AS BEGIN
	DECLARE @pessoaID INT
	DECLARE @disponibilidade INT

	SELECT @disponibilidade = disponibilidade FROM dbo.Equipamento WHERE equipamentoID = @equipamentoID
	IF (@disponibilidade = 0)
		RAISERROR('O Equipamento que escolheu n�o est� disponivel',16,1)
	ELSE
		BEGIN
			INSERT INTO dbo.Pessoa (nome) VALUES (@name)
			SET @pessoaID = (SELECT @@IDENTITY)
	
			INSERT INTO dbo.Cliente (pessoaID, nif, morada) VALUES (@pessoaID, @nif, @morada)

			INSERT INTO dbo.Aluguer (equipamentoID, codigoCliente, codigoEmpregado) VALUES (@equipamentoID, @pessoaID, @codigoEmpregado)
		END
END
GO

/*
Este procedimento armazenado serve para criar um cliente
*/
IF OBJECT_ID('sp_insertCliente') IS NOT NULL
DROP PROCEDURE sp_insertCliente;
GO
CREATE PROCEDURE sp_insertCliente
@name NVARCHAR(100) = NULL,
@nif INT = NULL,
@morada NVARCHAR(300) = NULL
AS BEGIN
	DECLARE @pessoaID INT
	INSERT INTO dbo.Pessoa VALUES (@name)
	SET @pessoaID = @@IDENTITY
	INSERT INTO dbo.Cliente (pessoaID, nif, morada) VALUES (@pessoaID, @nif, @morada)
END
GO

/*
Este procedimento armazenado serve para obter o id de um cliente, tanto pelo nome, como pelo NIF
*/
IF OBJECT_ID('sp_getPessoaID') IS NOT NULL
DROP PROCEDURE sp_getPessoaID;
GO
CREATE PROCEDURE sp_getPessoaID
@name NVARCHAR(100) = NULL,
@nif INT = NULL,
@pessoaID INT OUTPUT
AS BEGIN
	IF (@name IS NOT NULL)
		SELECT @pessoaID = pessoaID FROM dbo.Pessoa WHERE nome = @name
	ELSE
		BEGIN
			IF(@nif IS NOT NULL)
				SELECT @pessoaID = pessoaID FROM dbo.Cliente WHERE nif = @nif
		END
	RETURN
END
GO

/*
Este procedimento armazenado serve para alterar o nome de um cliente
*/
IF OBJECT_ID('sp_updateNomeCliente') IS NOT NULL
DROP PROCEDURE sp_updateNomeCliente;
GO
CREATE PROCEDURE sp_updateNomeCliente
@pid INT,
@newName NVARCHAR(100) = NULL
AS BEGIN
	DECLARE @pessoaID INT
	/*V� se o nome introduzido existe na tabela Pessoa*/
	SELECT @pessoaID = pessoaID FROM dbo.Pessoa WHERE pessoaID = @pid
	/*Se n�o existe n�o faz nada*/
	IF (@pessoaID IS NULL)
		BEGIN
			RAISERROR('O cliente n�o existe na base de dados',16,1)
		END
	ELSE
		BEGIN
			IF (@newName IS NOT NULL)
				BEGIN TRAN
				UPDATE dbo.Pessoa SET nome = @newName WHERE pessoaID = @pid;
				COMMIT TRAN
		END
END
GO

/*
Este procedimento armazenado serve para alterar o nif de um cliente
*/
IF OBJECT_ID('sp_updateNifCliente') IS NOT NULL
DROP PROCEDURE sp_updateNifCliente;
GO
CREATE PROCEDURE sp_updateNifCliente
@pid INT,
@newNif INT
AS BEGIN
	DECLARE @pessoaID INT
	/*V� se o nome introduzido existe na tabela Pessoa*/
	SELECT @pessoaID = pessoaID FROM dbo.Pessoa WHERE pessoaID = @pid
	/*Se n�o existe n�o faz nada*/
	IF @pessoaID IS NULL
		BEGIN
			RAISERROR('O cliente n�o existe na base de dados',16,1)
		END
	ELSE
		BEGIN
			IF (@newNif IS NOT NULL)
				BEGIN TRAN
				UPDATE dbo.Cliente SET nif = @newNif WHERE pessoaID = @pessoaID;
				COMMIT TRAN
		END
END
GO

/*
Este procedimento armazenado serve para alterar o nif de um cliente
*/
IF OBJECT_ID('sp_updateMoradaCliente') IS NOT NULL
DROP PROCEDURE sp_updateMoradaCliente;
GO
CREATE PROCEDURE sp_updateMoradaCliente
@pid INT,
@newMorada NVARCHAR(300)
AS BEGIN
	DECLARE @pessoaID INT
	/*V� se o nome introduzido existe na tabela Pessoa*/
	SELECT @pessoaID = pessoaID FROM dbo.Pessoa WHERE pessoaID = @pid
	/*Se n�o existe n�o faz nada*/
	IF @pessoaID IS NULL
		BEGIN
			RAISERROR('O cliente n�o existe na base de dados',16,1)
		END
	ELSE
		BEGIN
			IF (@newMorada IS NOT NULL)
				BEGIN TRAN
				UPDATE dbo.Cliente SET morada = @newMorada WHERE pessoaID = @pessoaID;
				COMMIT TRAN
		END
END
GO

/*
Este procedimento armazenado serve para alterar a morada de um cliente
*/
IF OBJECT_ID('sp_deleteCliente') IS NOT NULL
DROP PROCEDURE sp_deleteCliente
GO
CREATE PROCEDURE sp_deleteCliente
@pid INT
AS BEGIN
	DECLARE @pessoaID INT
	/*V� se o nome introduzido existe na tabela Pessoa*/
	SELECT @pessoaID = pessoaID FROM dbo.Pessoa WHERE pessoaID = @pid
	/*Se n�o existe n�o faz nada*/
	IF @pessoaID IS NULL
		BEGIN
			RAISERROR('O cliente que quer apagar n�o existe na base de dados',16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
			DELETE FROM dbo.Pessoa WHERE pessoaID = @pessoaID
			COMMIT TRAN
		END
END
GO

/*********************************ALUGUER*******************************************/
/*
Este procedimento armazenado serve para o cliente entregar o equipamento alugado
que gera atrav�s de um gatilho o tempo total que o cliente usufruiu do equipamento
*/
IF OBJECT_ID('sp_entrega_aluguer') IS NOT NULL
DROP PROCEDURE sp_entrega_aluguer;
GO
CREATE PROCEDURE sp_entrega_aluguer
@numeroSerie INT
AS BEGIN
	DECLARE @nsID INT
	SELECT @nsID = numeroSerie FROM dbo.Aluguer WHERE numeroSerie = @numeroSerie
	IF (@nsID IS NULL)
		BEGIN
			DECLARE @errormsg NVARCHAR(50)
			SET @errormsg = CONCAT('O n�mero de S�rie ',@numeroSerie,' n�o existe na BD')
			RAISERROR(@errormsg,16,1)
		END
	ELSE
		UPDATE dbo.Aluguer SET dataFim = GETDATE() WHERE numeroSerie = @numeroSerie
END
GO

/*
Este procedimento armazenado serve para apagar um aluguer
*/
IF OBJECT_ID('sp_deleteAluguer') IS NOT NULL
DROP PROCEDURE sp_deleteAluguer;
GO
CREATE PROCEDURE sp_deleteAluguer
@numerSerie INT
AS BEGIN
	DECLARE @nsID INT
	DECLARE @equipID INT
	DECLARE @dtFim DATE

	SELECT @nsID = numeroSerie,
		   @equipID = equipamentoID,
		   @dtFim = dataFim
	FROM dbo.Aluguer
	WHERE numeroSerie = @numerSerie
	
	IF (@nsID IS NULL)
		RAISERROR('O aluguer que quer apagar n�o existe na BD',16,1)
	ELSE
		BEGIN
			DELETE FROM dbo.Aluguer WHERE numeroSerie = @numerSerie
			IF (@dtFim IS NULL)
				UPDATE dbo.Equipamento
				SET disponibilidade = 1
				WHERE equipamentoID = @equipID
		END
END
GO

/*********************************EMPREGADO*******************************************/

/*
Este procedimento armazenado serve para criar um empregado
*/
IF OBJECT_ID('sp_insertEmpregado') IS NOT NULL
DROP PROCEDURE sp_insertEmpregado;
GO
CREATE PROCEDURE sp_insertEmpregado
@name NVARCHAR(100) = NULL
AS BEGIN
	DECLARE @pessoaID INT
	INSERT INTO dbo.Pessoa VALUES (@name)
	SET @pessoaID = @@IDENTITY
	INSERT INTO dbo.Empregado (pessoaID) VALUES (@pessoaID)
END
GO

/*********************************EQUIPAMENTO*******************************************/
/*
Este procedimento armazenado serve para introduzir um equipamento
*/
IF OBJECT_ID('sp_insertEquipamento') IS NOT NULL
DROP PROCEDURE sp_insertEquipamento
GO
CREATE PROCEDURE sp_insertEquipamento
@nomeTipo NVARCHAR(50),
@descricao NVARCHAR(300) = NULL,
@disponibilidade BIT = 1
AS BEGIN
	DECLARE @tipo NVARCHAR(50)
	/*V� se o nomeTipo introduzido existe na tabela Tipo*/
	SELECT @tipo = nomeTipo FROM dbo.Tipo WHERE nomeTipo = @nomeTipo
	/*Se n�o existe n�o faz nada*/
	IF @tipo IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('O tipo de equipamento ',@nomeTipo,' n�o existe na BD')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
			INSERT INTO dbo.Equipamento (nomeTipo, descricao, disponibilidade) VALUES (@tipo, @descricao, @disponibilidade)
			COMMIT TRAN
		END
END
GO

/*
Este procedimento armazenado serve para alterar o nomeTipo de um equipamento
*/
IF OBJECT_ID('sp_updateEquipamento') IS NOT NULL
DROP PROCEDURE sp_updateEquipamento
GO
CREATE PROCEDURE sp_updateEquipamento
@id INT,
@nomeTipo NVARCHAR(50),
@descricao NVARCHAR(300) = NULL,
@disponibilidade BIT = 1
AS BEGIN
	DECLARE @tipo NVARCHAR(50)
	/*V� se o nomeTipo introduzido existe na tabela Tipo*/
	SELECT @tipo = nomeTipo FROM dbo.Tipo WHERE nomeTipo = @nomeTipo
	/*Se n�o existe n�o faz nada*/
	IF @tipo IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('O tipo de equipamento ',@nomeTipo,' n�o existe na BD')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
			UPDATE dbo.Equipamento SET nomeTipo = @nomeTipo, descricao = @descricao, disponibilidade = @disponibilidade WHERE equipamentoID = @id
			COMMIT TRAN
		END
END
GO

/*
Este procedimento armazenado serve para alterar a disponibilidade de um equipamento
*/
IF OBJECT_ID('sp_updateDisponibilidadeEquipamento') IS NOT NULL
DROP PROCEDURE sp_updateDisponibilidadeEquipamento
GO
CREATE PROCEDURE sp_updateDisponibilidadeEquipamento
@id INT,
@disponibilidade BIT
AS BEGIN
	DECLARE @eid INT
	/*V� se o eqipamentoID introduzido existe na tabela Equipamento*/
	SELECT @eid = equipamentoID FROM dbo.Equipamento WHERE equipamentoID = @id
	/*Se n�o existe n�o faz nada*/
	IF @eid IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('O equipamento com o ID ',@id,' n�o existe na BD')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
			UPDATE dbo.Equipamento SET disponibilidade = @disponibilidade WHERE equipamentoID = @id
			COMMIT TRAN
		END
END
GO

/*
Este procedimento armazenado serve para remover um equipamento
*/
IF OBJECT_ID('sp_deleteEquipamento') IS NOT NULL
DROP PROCEDURE sp_deleteEquipamento
GO
CREATE PROCEDURE sp_deleteEquipamento
@id INT
AS BEGIN
	DECLARE @idEquipamento INT
	/*V� se o nomeTipo introduzido existe na tabela Tipo*/
	SELECT @idEquipamento = equipamentoID FROM dbo.Equipamento WHERE equipamentoID = @id
	/*Se n�o existe n�o faz nada*/
	IF @idEquipamento IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('O equipamento com o ID ',@id,' n�o existe na BD')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
			DELETE FROM dbo.Equipamento WHERE equipamentoID = @id
			COMMIT TRAN
		END
END
GO

/*********************************PRECO*******************************************/
/*
Este procedimento armazenado serve para actualizar o pre�o para uma frac��o de tempo
para um equipamento em particular
*/
IF OBJECT_ID('sp_updateValorPreco') IS NOT NULL
DROP PROCEDURE sp_updateValorPreco
GO
CREATE PROCEDURE sp_updateValorPreco
@nomeTipo NVARCHAR(50),
@fraccao INT,
@valor INT
AS BEGIN
	DECLARE @errormsg NVARCHAR(50)
	DECLARE @ntipo NVARCHAR(50)
	DECLARE @frac INT
	SELECT @frac = fraccao FROM dbo.Preco WHERE (fraccao = @fraccao AND nomeTipo = @nomeTipo)
	SELECT @ntipo = nomeTipo FROM dbo.Preco WHERE nomeTipo = @nomeTipo
	IF (@valor <= 0)
		BEGIN
			SET @errormsg = CONCAT('N�o pode atribuir ',@valor,' como pre�o')
			RAISERROR(@errormsg, 16, 1)
		END
	IF (@frac IS NULL)
		BEGIN
			SET @errormsg = CONCAT('A frac��o ',@fraccao,' n�o � v�lida')
			RAISERROR(@errormsg, 16, 1)
		END
	IF (@ntipo IS NULL)
		BEGIN
			SET @errormsg = CONCAT('O tipo de Equipamento ',@nomeTipo,' n�o existe na BD')
			RAISERROR(@errormsg, 16, 1)
		END
	IF(@valor > 0 AND @frac IS NOT NULL AND @ntipo IS NOT NULL)
		BEGIN
			UPDATE dbo.Preco SET valor = @valor WHERE (nomeTipo = @nomeTipo AND fraccao = @fraccao)
		END
END
GO

/*
Este procedimento armazenado serve para actualizar uma dada frac��o para um dado equipamento
*/
IF OBJECT_ID('sp_updateFraccaoPreco') IS NOT NULL
DROP PROCEDURE sp_updateFraccaoPreco
GO
CREATE PROCEDURE sp_updateFraccaoPreco
@nomeTipo NVARCHAR(50),
@fraccao_antiga INT,
@fraccao_nova INT
AS BEGIN
	DECLARE @errormsg NVARCHAR(50)
	DECLARE @ntipo NVARCHAR(50)
	DECLARE @frac INT
	SELECT @frac = fraccao FROM dbo.Preco WHERE (fraccao = @fraccao_antiga AND nomeTipo = @nomeTipo)
	SELECT @ntipo = nomeTipo FROM dbo.Preco WHERE nomeTipo = @nomeTipo

	IF (@frac IS NULL)
		BEGIN
			SET @errormsg = CONCAT('A frac��o ',@fraccao_antiga,' n�o � v�lida')
			RAISERROR(@errormsg, 16, 1)
		END
	IF (@ntipo IS NULL)
		BEGIN
			SET @errormsg = CONCAT('O tipo de Equipamento ',@nomeTipo,' n�o existe na BD')
			RAISERROR(@errormsg, 16, 1)
		END
	IF(@frac IS NOT NULL AND @ntipo IS NOT NULL)
		BEGIN
			UPDATE dbo.Preco SET fraccao = @fraccao_nova WHERE (nomeTipo = @nomeTipo AND fraccao = @fraccao_antiga)
		END
END
GO

/*********************************PROMOCAO*******************************************/
/*
Este procedimento armazenado serve para inserir uma promo��o
*/
IF OBJECT_ID('sp_insertPromocao') IS NOT NULL
DROP PROCEDURE sp_insertPromocao
GO
CREATE PROCEDURE sp_insertPromocao
@dataInicio DATE,
@dataFim DATE,
@descontoPrecoPorCento INT = NULL,
@descontoFraccao INT = NULL,
@descricao nvarchar(300) = NULL
AS BEGIN
	IF (@dataInicio IS NULL AND @dataFim IS NULL) OR (@dataInicio > @dataFim)
		BEGIN
			RAISERROR('Erro na inser��o das datas',16,1)
		END
	ELSE IF (@descontoPrecoPorCento IS NULL AND @descontoFraccao IS NULL) OR (@descontoPrecoPorCento IS NOT NULL AND @descontoFraccao IS NOT NULL)
		BEGIN
			RAISERROR('Tem de haver um e s� um tipo de desconto',16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
			INSERT INTO dbo.Promocao (dataInicio, dataFim, descontoPrecoPorCento, descontoFraccao, descricao)
						VALUES (@dataInicio, @dataFim, @descontoPrecoPorCento, @descontoFraccao, @descricao)
			COMMIT TRAN
		END
END
GO

/*
Este procedimento armazenado serve para atualizar uma promo��o
*/
IF OBJECT_ID('sp_updatePromocao') IS NOT NULL
DROP PROCEDURE sp_updatePromocao
GO
CREATE PROCEDURE sp_updatePromocao
@id INT,
@dataInicio DATE,
@dataFim DATE,
@descontoPrecoPorCento INT = NULL,
@descontoFraccao INT = NULL,
@descricao nvarchar(300) = NULL
AS BEGIN
	IF (@dataInicio = '' OR @dataFim  = '') OR (@dataInicio > @dataFim)
		BEGIN
			RAISERROR('Erro na inser��o das datas',16,1)
		END
	ELSE IF (@descontoPrecoPorCento IS NULL AND @descontoFraccao IS NULL) OR (@descontoPrecoPorCento IS NOT NULL AND @descontoFraccao IS NOT NULL)
		BEGIN
			RAISERROR('Tem de haver um e s� um tipo de desconto',16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
				UPDATE dbo.Promocao 
				SET dataInicio = @dataInicio, dataFim = @dataFim, descontoPrecoPorCento = @descontoPrecoPorCento,
					descontoFraccao = @descontoFraccao, descricao = @descricao
				WHERE promocaoID = @id
			COMMIT TRAN
		END
END
GO

/*
Este procedimento armazenado serve para remover uma promo��o
*/
IF OBJECT_ID('sp_removePromocao') IS NOT NULL
DROP PROCEDURE sp_removePromocao
GO
CREATE PROCEDURE sp_removePromocao
@id INT
AS BEGIN
	DECLARE @idProm INT
	/*V� se o promo��o existe na tabela Promo��o*/
	SELECT @idProm = promocaoID FROM dbo.Promocao WHERE promocaoID = @id
	/*Se n�o existe n�o faz nada*/
	IF @idProm IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('A promo��o com o ID ',@id,' n�o existe na BD')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
				DELETE FROM dbo.Promocao WHERE promocaoID = @id
			COMMIT TRAN
		END
END
GO

/**************************************************************************** INSERT INTO TABLE ***********************************************************************/

INSERT INTO dbo.Promocao (dataInicio, dataFim, descontoPrecoPorCento, descontoFraccao, descricao) VALUES 
						 ('2016-01-01', '2016-12-31', 5, NULL, 'Desconto de 5% todo o ano de 2016'), 
						 ('2016-06-01', '2016-08-31', NULL, 30, 'Desconto de 30 minutos na frac��o durante o Ver�o de 2016'),
						 ('2016-05-01', '2016-05-31', 20, NULL, 'Desconto de 20% no m�s de Maio');

INSERT INTO dbo.Tipo	(nomeTipo, promocaoID, descricao) VALUES 
						 ('Canoa', 1, 'Canoa'), 
						 ('Gaivota', 2, 'Gaivota'),
						 ('Toldo', 3, 'Toldo'),
						 ('Barco', NULL, 'Barco n�o tem promo��es');

DECLARE @exampleDate date = DATEADD(DAY, 30, GETDATE())
INSERT INTO dbo.Preco	(fraccao, valor, validade, nomeTipo) VALUES 
						 (30, 20, @exampleDate, 'Canoa'),
						 (60, 35, @exampleDate, 'Canoa'),
						 (90, 50, @exampleDate, 'Canoa'),
						 (8*60, 120, @exampleDate, 'Canoa'),
						 (30, 25, @exampleDate, 'Gaivota'),
						 (60, 45, @exampleDate, 'Gaivota'),
						 (90, 60, @exampleDate, 'Gaivota'),
						 (8*60, 135, @exampleDate, 'Gaivota'),
						 (2*60, 20, @exampleDate, 'Toldo'),
						 (4*60, 35, @exampleDate, 'Toldo'),
						 (6*60, 50, @exampleDate, 'Toldo'),
						 (8*60, 80, @exampleDate, 'Toldo'),
						 (30, 60, @exampleDate, 'Barco'),
						 (60, 100, @exampleDate, 'Barco'),
						 (90, 125, @exampleDate, 'Barco'),
						 (4*60, 250, @exampleDate, 'Barco'),
						 (8*60, 500, @exampleDate, 'Barco');

/*INSERT de Equipamentos com Procedures*/
EXEC dbo.sp_insertEquipamento 'Canoa','Canoa n�1', 1
EXEC dbo.sp_insertEquipamento 'Canoa','Canoa n�2', 1
EXEC dbo.sp_insertEquipamento 'Canoa','Canoa n�3', 1
EXEC dbo.sp_insertEquipamento 'Canoa','Canoa n�4', 1
EXEC dbo.sp_insertEquipamento 'Canoa','Canoa n�5', 1
EXEC dbo.sp_insertEquipamento 'Canoa','Canoa n�6', 1
EXEC dbo.sp_insertEquipamento 'Gaivota','Gaivota n�1', 1
EXEC dbo.sp_insertEquipamento 'Gaivota','Gaivota n�2', 1
EXEC dbo.sp_insertEquipamento 'Gaivota','Gaivota n�3', 1
EXEC dbo.sp_insertEquipamento 'Gaivota','Gaivota n�4', 1
EXEC dbo.sp_insertEquipamento 'Gaivota','Gaivota n�5', 1
EXEC dbo.sp_insertEquipamento 'Toldo','Toldo n�1', 1
EXEC dbo.sp_insertEquipamento 'Toldo','Toldo n�2', 1
EXEC dbo.sp_insertEquipamento 'Toldo','Toldo n�3', 1
EXEC dbo.sp_insertEquipamento 'Toldo','Toldo n�4', 1
EXEC dbo.sp_insertEquipamento 'Toldo','Toldo n�5', 1
EXEC dbo.sp_insertEquipamento 'Barco','Barco n�1', 1
EXEC dbo.sp_insertEquipamento 'Barco','Barco n�2', 1
EXEC dbo.sp_insertEquipamento 'Barco','Barco n�3', 1
EXEC dbo.sp_insertEquipamento 'Barco','Barco n�4', 1
EXEC dbo.sp_insertEquipamento 'Barco','Barco n�5', 1
EXEC dbo.sp_insertEquipamento 'Barco','Barco n�6', 1
EXEC dbo.sp_insertEquipamento 'Barco','Barco n�7', 1

/*INSERT de Empregados com Procedures*/
EXEC dbo.sp_insertEmpregado 'Rafael Machado'
EXEC dbo.sp_insertEmpregado 'F�bio Martin'
EXEC dbo.sp_insertEmpregado 'Ana Luz'
EXEC dbo.sp_insertEmpregado 'Tiago Loureiro'
EXEC dbo.sp_insertEmpregado 'Andr� Carona'

/*INSERT de Clientes com Procedures*/
EXEC dbo.sp_insertCliente 'Consumidor Final', 999999999, 'Sem Morada'
EXEC dbo.sp_insertCliente 'Rafael Macedo', 236394592, 'Avenida da Liberdade'
EXEC dbo.sp_insertCliente 'Jo�o Silva', 125896574, 'Avenida Marechal Gomes da Costa'
EXEC dbo.sp_insertCliente 'Luis Pinto', 365887456, 'Avenida Jo�o XXI'
EXEC dbo.sp_insertCliente 'Bruno Louren�o', 125774562, 'Rua da Bela Vista a Gra�a'
EXEC dbo.sp_insertCliente 'Raimunda Frigida', 123654789, 'Praceta Rog�rio Paulo'
EXEC dbo.sp_insertCliente 'Joana Castanheira', 125863257, 'Praceta Irene Lisboa'
EXEC dbo.sp_insertCliente 'Jo�o Castanho', 255644856, 'Avenida do Ocean�rio'
EXEC dbo.sp_insertCliente 'Vladimiro Putin', 254621456, 'Avenida Bocage'
EXEC dbo.sp_insertCliente 'Carlos Lopes', 125874523, 'Avenida das T�lipas'
EXEC dbo.sp_insertCliente 'Helena Marques', 125447852, 'Avenida 24 de Julho'
EXEC dbo.sp_insertCliente 'Barack Obama', 123669554, 'Pra�a do Municipio'
EXEC dbo.sp_insertCliente 'Ana Rita Pereira', 236566587, 'Avenida dos Bombeiros Volunt�rios'
EXEC dbo.sp_insertCliente 'Francisco Lopes', 231456987, 'Rua Serpa Pinto'
EXEC dbo.sp_insertCliente 'Tiago Louren�o', 125478541, 'Rua do Sapateiro'
EXEC dbo.sp_insertCliente 'J�lia Pereira', 121114521, 'Rua do Mercado de Campo de Ourique'
EXEC dbo.sp_insertCliente NULL, NULL, NULL
EXEC dbo.sp_insertCliente NULL, NULL, NULL

/*INSERT de Alugueres com Procedures*/ 
EXEC dbo.sp_aluguer_clienteRegistado 6, 23, 1
EXEC dbo.sp_aluguer_clienteRegistado 11, 16, 2
EXEC dbo.sp_aluguer_clienteRegistado 16, 9, 3
EXEC dbo.sp_aluguer_clienteRegistado 14, 12, 4
EXEC dbo.sp_aluguer_clienteRegistado 9, 21, 5
EXEC dbo.sp_aluguer_clienteRegistado 7, 17, 3


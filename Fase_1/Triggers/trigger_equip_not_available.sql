/**************************************************************************** TRIGGERS ***********************************************************************/
/*
Este trigger coloca o equipamento indispon�vel ap�s o ato de aluguer
*/
IF OBJECT_ID('tr_equip_not_available') IS NOT NULL
DROP TRIGGER tr_equip_not_available;
GO
CREATE TRIGGER tr_equip_not_available
ON dbo.Aluguer
AFTER INSERT
AS BEGIN
	SET NOCOUNT ON;
	BEGIN
		UPDATE dbo.Equipamento
		SET disponibilidade = 0
		WHERE equipamentoID = (SELECT equipamentoID FROM inserted)
	END
END
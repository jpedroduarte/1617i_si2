/**************************************************************************** TRIGGERS ***********************************************************************/
/*
Este trigger faz update na propria tabela, apenas quando o campo 'dataFim' � alterado,
alterando o campo 'duracao' que � a diferen�a entre a data de inicio e a data de fim
em minutos.
*/
IF OBJECT_ID('tr_updateValor') IS NOT NULL
DROP TRIGGER tr_updateValor;
GO
CREATE TRIGGER tr_updateValor
ON dbo.Aluguer
AFTER UPDATE
AS BEGIN
	SET NOCOUNT ON;
	DECLARE @nSerie AS INT
	DECLARE @desc AS BIT
	IF UPDATE(dataFim)
	SET @nSerie = (SELECT numeroSerie FROM inserted)
	BEGIN
		UPDATE Aluguer 
		SET duracao = (SELECT DATEDIFF(MINUTE, (SELECT dataHora FROM inserted), (SELECT dataFim FROM inserted)))
		WHERE numeroSerie = (SELECT numeroSerie FROM inserted)

		UPDATE dbo.Equipamento
		SET disponibilidade = 1
		WHERE equipamentoID = (SELECT equipamentoID FROM inserted)

		SELECT CAST(
					 CASE 
						  WHEN P.descontoPrecoPorCento IS NOT NULL AND P.descontoFraccao IS NULL 
						  THEN 1 
						  ELSE 0 
					 END AS bit) AS Desconto,
			   A.numeroSerie,
			   A.duracao,
			   E.descricao,
			   T.nomeTipo,
			   T.promocaoID,
			   P.descontoPrecoPorCento,
			   P.descontoFraccao,
			   PR.valor,
			   PR.fraccao INTO #DATA_FIM
		FROM dbo.Aluguer AS A
		INNER JOIN dbo.Equipamento AS E
		ON (A.equipamentoID = E.equipamentoID)
		INNER JOIN dbo.Tipo AS T
		ON (E.nomeTipo = T.nomeTipo)
		LEFT JOIN dbo.Promocao AS P
		ON (T.promocaoID = P.promocaoID)
		INNER JOIN dbo.Preco AS PR
		ON (T.nomeTipo = PR.nomeTipo)
		WHERE A.numeroSerie = @nSerie
		AND PR.valor = (SELECT dbo.get_price_from_aluguer_time(T.nomeTipo, A.duracao))

		SELECT @desc = Desconto FROM #DATA_FIM
		
		DECLARE @valor AS DECIMAL(6, 2)
		IF @desc = 1
			DECLARE @percent AS INT
			SELECT @valor = valor FROM #DATA_FIM
			SELECT @percent = descontoPrecoPorCento FROM #DATA_FIM

			UPDATE dbo.Aluguer
			SET valor  = @valor - (@valor * @percent / 100)
			WHERE numeroSerie = @nSerie
		ELSE IF @desc = 0
			DECLARE @descFracao AS INT
			DECLARE @fracao AS INT
			DECLARE @nomeT AS NVARCHAR(50)

			SELECT @fracao = fraccao FROM #DATA_FIM;
			SELECT @descFracao = descontoFraccao FROM #DATA_FIM
			SELECT @nomeT = nomeTipo FROM #DATA_FIM

			UPDATE dbo.Aluguer
			SET valor = (SELECT dbo.get_price_from_aluguer_time(@nomeT, @fracao - @descFracao))				
			WHERE numeroSerie = @nSerie
		ELSE
			UPDATE dbo.Aluguer
			SET valor = (SELECT valor FROM #DATA_FIM)				
			WHERE numeroSerie = @nSerie
	END
	IF OBJECT_ID('tempdb..#DATA_FIM') IS NOT NULL
		DROP TABLE #DATA_FIM
END
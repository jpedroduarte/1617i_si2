/*Esta view mostra os equipamentos dispon�veis de um determinado tipo e dentro de um determinado per�odo

  Optou-se por usar uma function, em vez de view ou stored procedure, porque s� precisamos de listar e passar
  como parametros o tipo e periodo
*/

CREATE FUNCTION dbo.equip_livres_tempo_tipo
(
@dataInicio AS date,
@dataFim AS date,
@tipo AS nvarchar(50)
)
RETURNS TABLE

	RETURN
	(
		SELECT Equipamento.*
		FROM Equipamento
		WHERE equipamentoID NOT IN (
			SELECT A.equipamentoID
			FROM dbo.Aluguer AS A
			INNER JOIN dbo.Equipamento AS E
			ON (A.equipamentoID = E.equipamentoID)
			WHERE A.dataHora BETWEEN @dataInicio AND @dataFim
			OR A.dataFim IS NULL
		)
		AND nomeTipo = @tipo
	)
;

DROP FUNCTION dbo.equip_livres_tempo_tipo

SELECT * FROM dbo.equip_livres_tempo_tipo(DATEADD(DAY, -9, GETDATE()), DATEADD(DAY, 1, GETDATE()), 'Barco')
select * from dbo.Aluguer
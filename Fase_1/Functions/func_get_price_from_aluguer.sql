use AEnima

if OBJECT_ID('get_price_from_aluguer_time') is not null
	drop function get_price_from_aluguer_time

go

/**
*	Function - Get price from closest timestamp in Prices table
*/

create function dbo.get_price_from_aluguer_time
(
@tipo as nvarchar(MAX),
@time as int
)
returns table
as
	return 
	(
		select top 1 p.fraccao, p.valor from dbo.Preco as p
		where nomeTipo = @tipo and fraccao >= @time and validade > GETDATE()
		order by abs(fraccao - @time)
	)

/**
*Test
*/

go

--select dbo.get_price_from_aluguer_time('Barco', 50)
select * from dbo.get_price_from_aluguer_time('Barco', 59)
select * from dbo.Preco where nomeTipo='Barco'  and validade > GETDATE()

--INSERT INTO dbo.Preco	(fraccao, valor, validade, nomeTipo) VALUES (10, 20, DATEADD(DAY, -10, GETDATE()), 'Barco')

/* First aproach
select * from dbo.Preco
where nomeTipo='Barco'
order by abs(fraccao - 46)
*/
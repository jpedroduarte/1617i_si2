/*
Este procedimento armazenado serve para atualizar uma promo��o
*/
IF OBJECT_ID('sp_updatePromocao') IS NOT NULL
DROP PROCEDURE sp_updatePromocao
GO
CREATE PROCEDURE sp_updatePromocao
@id INT,
@dataInicio DATE,
@dataFim DATE,
@descontoPrecoPorCento INT = NULL,
@descontoFraccao INT = NULL,
@descricao nvarchar(300) = NULL
AS BEGIN
	IF (@dataInicio = '' OR @dataFim  = '') OR (@dataInicio > @dataFim)
		BEGIN
			RAISERROR('Erro na inser��o das datas',16,1)
		END
	ELSE IF (@descontoPrecoPorCento IS NULL AND @descontoFraccao IS NULL) OR (@descontoPrecoPorCento IS NOT NULL AND @descontoFraccao IS NOT NULL)
		BEGIN
			RAISERROR('Tem de haver um e s� um tipo de desconto',16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
				UPDATE dbo.Promocao 
				SET dataInicio = @dataInicio, dataFim = @dataFim, descontoPrecoPorCento = @descontoPrecoPorCento,
					descontoFraccao = @descontoFraccao, descricao = @descricao
				WHERE promocaoID = @id
			COMMIT TRAN
		END
END

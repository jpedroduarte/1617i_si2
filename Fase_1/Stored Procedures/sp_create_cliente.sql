/*
Este procedimento armazenado serve para criar um cliente
*/
IF OBJECT_ID('sp_createCliente') IS NOT NULL
DROP PROCEDURE sp_createCliente;
GO
CREATE PROCEDURE sp_createCliente
@name NVARCHAR(100) = NULL,
@nif INT = NULL,
@morada NVARCHAR(300) = NULL
AS BEGIN
	DECLARE @pessoaID INT
	INSERT INTO dbo.Pessoa VALUES (@name)
	SET @pessoaID = @@IDENTITY
	INSERT INTO dbo.Cliente (pessoaID, nif, morada) VALUES (@pessoaID, @nif, @morada)
END
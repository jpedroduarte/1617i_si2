/*
Este procedimento armazenado serve para introduzir um equipamento
*/
IF OBJECT_ID('sp_insertEquipamento') IS NOT NULL
DROP PROCEDURE sp_insertEquipamento
GO
CREATE PROCEDURE sp_insertEquipamento
@nomeTipo NVARCHAR(50),
@descricao NVARCHAR(300) = NULL
AS BEGIN
	DECLARE @tipo NVARCHAR(50)
	/*V� se o nome introduzido existe na tabela Pessoa*/
	SELECT @tipo = nomeTipo FROM dbo.Tipo WHERE nomeTipo = @nomeTipo
	/*Se n�o existe n�o faz nada*/
	IF @tipo IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('O tipo de equipamento ',@nomeTipo,' n�o existe na BD')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
			INSERT INTO dbo.Equipamento (nomeTipo, descricao) VALUES (@tipo, @descricao)
			COMMIT TRAN
		END
END
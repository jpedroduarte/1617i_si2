USE AEnima
IF OBJECT_ID('sp_updateValorPreco') IS NOT NULL
DROP PROCEDURE sp_updateFraccaoPreco
GO
-- Alterar a fraccao de um Preco
CREATE PROCEDURE sp_updateFraccaoPreco
@nomeTipo NVARCHAR(50),
@fraccao_antiga INT,
@fraccao_nova INT
AS BEGIN
	DECLARE @errormsg NVARCHAR(50)
	DECLARE @ntipo NVARCHAR(50)
	DECLARE @frac INT
	SELECT @frac = fraccao FROM dbo.Preco WHERE (fraccao = @fraccao_antiga AND nomeTipo = @nomeTipo)
	SELECT @ntipo = nomeTipo FROM dbo.Preco WHERE nomeTipo = @nomeTipo

	IF (@frac IS NULL)
		BEGIN
			SET @errormsg = CONCAT('A frac��o ',@fraccao_antiga,' n�o � v�lida')
			RAISERROR(@errormsg, 16, 1)
		END
	IF (@ntipo IS NULL)
		BEGIN
			SET @errormsg = CONCAT('O tipo de Equipamento ',@nomeTipo,' n�o existe na BD')
			RAISERROR(@errormsg, 16, 1)
		END
	IF(@frac IS NOT NULL AND @ntipo IS NOT NULL)
		BEGIN
			UPDATE dbo.Preco SET fraccao = @fraccao_nova WHERE (nomeTipo = @nomeTipo AND fraccao = @fraccao_antiga)
		END
END

/**
* Tests
*/
GO
select * from Preco where nomeTipo= 'Barco'
--EXEC dbo.sp_updateFraccaoPreco 'Barco', 40, 30
EXEC dbo.sp_updateFraccaoPreco 'Barco', 30, 40
select * from Preco where nomeTipo= 'Barco'

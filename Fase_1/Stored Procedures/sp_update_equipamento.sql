
/*
Este procedimento armazenado serve para alterar o nomeTipo de um equipamento
*/
IF OBJECT_ID('sp_updateEquipamento') IS NOT NULL
DROP PROCEDURE sp_updateEquipamento
GO
CREATE PROCEDURE sp_updateEquipamento
@id INT,
@nomeTipo NVARCHAR(50),
@descricao NVARCHAR(300) = NULL
AS BEGIN
	DECLARE @tipo NVARCHAR(50)
	/*V� se o nome introduzido existe na tabela Pessoa*/
	SELECT @tipo = nomeTipo FROM dbo.Tipo WHERE nomeTipo = @nomeTipo
	/*Se n�o existe n�o faz nada*/
	IF @tipo IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('O tipo de equipamento ',@nomeTipo,' n�o existe na BD')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
			UPDATE dbo.Equipamento SET nomeTipo = @nomeTipo, descricao = @descricao WHERE equipamentoID = @id
			COMMIT TRAN
		END
END
/*
Este procedimento armazenado serve para inserir uma promo��o
*/
IF OBJECT_ID('sp_insertPromocao') IS NOT NULL
DROP PROCEDURE sp_insertPromocao
GO
CREATE PROCEDURE sp_insertPromocao
@dataInicio DATE,
@dataFim DATE,
@descontoPrecoPorCento INT = NULL,
@descontoFraccao INT = NULL,
@descricao nvarchar(300) = NULL
AS BEGIN
	IF (@dataInicio IS NULL AND @dataFim IS NULL) OR (@dataInicio > @dataFim)
		BEGIN
			RAISERROR('Erro na inser��o das datas',16,1)
		END
	ELSE IF (@descontoPrecoPorCento IS NULL AND @descontoFraccao IS NULL) OR (@descontoPrecoPorCento IS NOT NULL AND @descontoFraccao IS NOT NULL)
		BEGIN
			RAISERROR('Tem de haver um e s� um tipo de desconto',16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
			INSERT INTO dbo.Promocao (dataInicio, dataFim, descontoPrecoPorCento, descontoFraccao, descricao)
						VALUES (@dataInicio, @dataFim, @descontoPrecoPorCento, @descontoFraccao, @descricao)
			COMMIT TRAN
		END
END
GO

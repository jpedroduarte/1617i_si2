
/*
Este procedimento armazenado serve para alterar o nif de um cliente
*/
IF OBJECT_ID('sp_updateMoradaCliente') IS NOT NULL
DROP PROCEDURE sp_updateMoradaCliente;
GO
CREATE PROCEDURE sp_updateMoradaCliente
@name NVARCHAR(100) = 'Consumidor Final',
@newMorada NVARCHAR(300) = 'Sem morada'
AS BEGIN
	DECLARE @pessoaID INT
	/*V� se o nome introduzido existe na tabela Pessoa*/
	SELECT @pessoaID = pessoaID FROM dbo.Pessoa WHERE nome = @name
	/*Se n�o existe n�o faz nada*/
	IF @pessoaID IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('O nome ',@name,' n�o existe na base de dados')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			IF (@newMorada IS NOT NULL)
				BEGIN TRAN
				UPDATE dbo.Cliente SET morada = @newMorada WHERE pessoaID = @pessoaID;
				COMMIT TRAN
		END
END
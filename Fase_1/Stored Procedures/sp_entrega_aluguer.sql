/*
Este procedimento armazenado serve para o cliente entregar o equipamento alugado
que gera atrav�s de um gatilho o tempo total que o cliente usufruiu do equipamento
*/
IF OBJECT_ID('sp_entrega_aluguer') IS NOT NULL
DROP PROCEDURE sp_entrega_aluguer;
GO
CREATE PROCEDURE sp_entrega_aluguer
@numeroSerie INT
AS BEGIN
	UPDATE dbo.Aluguer SET dataFim = GETDATE() WHERE numeroSerie = @numeroSerie
END
/*
Este procedimento armazenado serve para remover um equipamento
*/
IF OBJECT_ID('sp_removeEquipamento') IS NOT NULL
DROP PROCEDURE sp_removeEquipamento
GO
CREATE PROCEDURE sp_removeEquipamento
@id INT
AS BEGIN
	DECLARE @idEquipamento INT
	/*V� se o Equipamento existe na tabela Equipamento*/
	SELECT @idEquipamento = equipamentoID FROM dbo.Equipamento WHERE equipamentoID = @id
	/*Se n�o existe n�o faz nada*/
	IF @idEquipamento IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('O equipamento com o ID ',@id,' n�o existe na BD')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
			DELETE FROM dbo.Equipamento WHERE equipamentoID = @id
			COMMIT TRAN
		END
END
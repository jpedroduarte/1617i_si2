/*
Este procedimento armazenado serve para alterar o nome de um cliente
*/
IF OBJECT_ID('sp_updateNomeCliente') IS NOT NULL
DROP PROCEDURE sp_updateNomeCliente;
GO
CREATE PROCEDURE sp_updateNomeCliente
@name NVARCHAR(100) = 'Consumidor Final',
@newName NVARCHAR(100) = NULL
AS BEGIN
	DECLARE @pessoaID INT
	/*V� se o nome introduzido existe na tabela Pessoa*/
	SELECT @pessoaID = pessoaID FROM dbo.Pessoa WHERE nome = @name
	/*Se n�o existe n�o faz nada*/
	IF (@pessoaID IS NULL)
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('O nome ',@name,' n�o existe na base de dados')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			IF (@newName IS NOT NULL)
				BEGIN TRAN
				UPDATE dbo.Pessoa SET nome = @newName WHERE nome = @name;
				COMMIT TRAN
		END
END
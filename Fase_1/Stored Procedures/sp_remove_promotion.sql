/*
Este procedimento armazenado serve para remover uma promo��o
*/
IF OBJECT_ID('sp_removePromocao') IS NOT NULL
DROP PROCEDURE sp_removePromocao
GO
CREATE PROCEDURE sp_removePromocao
@id INT
AS BEGIN
	DECLARE @idProm INT
	/*V� se o promo��o existe na tabela Promo��o*/
	SELECT @idProm = promocaoID FROM dbo.Promocao WHERE promocaoID = @id
	/*Se n�o existe n�o faz nada*/
	IF @idProm IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('A promo��o com o ID ',@id,' n�o existe na BD')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			BEGIN TRAN
				DELETE FROM dbo.Promocao WHERE promocaoID = @id
			COMMIT TRAN
		END
END
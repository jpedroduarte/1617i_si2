/**************************************************************************** STORED PROCEDURES ***********************************************************************/
/*
Este procedimento armazenado serve para criar um aluguer
quando n�o existe um cliente criado, podendo-se criar na
mesma altura
*/
IF OBJECT_ID('sp_aluguer_clienteNaoRegistado') IS NOT NULL
DROP PROCEDURE sp_aluguer_clienteNaoRegistado;
GO
CREATE PROCEDURE sp_aluguer_clienteNaoRegistado
@name NVARCHAR(100),
@nif INT,
@morada NVARCHAR(300),
@equipamentoID INT,
@codigoEmpregado INT
AS BEGIN
	DECLARE @pessoaID INT
	DECLARE @clienteID INT

	INSERT INTO dbo.Pessoa (nome) VALUES (@name)
	SET @pessoaID = (SELECT @@IDENTITY)
	
	INSERT INTO dbo.Cliente (pessoaID, nif, morada) VALUES (@pessoaID, @nif, @morada)
	SET @clienteID = (SELECT @@IDENTITY)

	INSERT INTO dbo.Aluguer (equipamentoID, codigoCliente, codigoEmpregado) VALUES (@equipamentoID, @clienteID, @codigoEmpregado)
END
IF OBJECT_ID('sp_updateValorPreco') IS NOT NULL
DROP PROCEDURE sp_updateValorPreco
GO
CREATE PROCEDURE sp_updateValorPreco
@nomeTipo NVARCHAR(50),
@fraccao INT,
@valor INT
AS BEGIN
	DECLARE @errormsg NVARCHAR(50)
	DECLARE @ntipo NVARCHAR(50)
	DECLARE @frac INT
	SELECT @frac = fraccao FROM dbo.Preco WHERE (fraccao = @fraccao AND nomeTipo = @nomeTipo)
	SELECT @ntipo = nomeTipo FROM dbo.Preco WHERE nomeTipo = @nomeTipo
	IF (@valor <= 0)
		BEGIN
			SET @errormsg = CONCAT('N�o pode atribuir ',@valor,' como pre�o')
			RAISERROR(@errormsg, 16, 1)
		END
	IF (@frac IS NULL)
		BEGIN
			SET @errormsg = CONCAT('A frac��o ',@fraccao,' n�o � v�lida')
			RAISERROR(@errormsg, 16, 1)
		END
	IF (@ntipo IS NULL)
		BEGIN
			SET @errormsg = CONCAT('O tipo de Equipamento ',@nomeTipo,' n�o existe na BD')
			RAISERROR(@errormsg, 16, 1)
		END
	IF(@valor > 0 AND @frac IS NOT NULL AND @ntipo IS NOT NULL)
		BEGIN
			UPDATE dbo.Preco SET valor = @valor WHERE (nomeTipo = @nomeTipo AND fraccao = @fraccao)
		END
END
/*
Este procedimento armazenado serve para alterar o nif de um cliente
*/
IF OBJECT_ID('sp_updateNifCliente') IS NOT NULL
DROP PROCEDURE sp_updateNifCliente;
GO
CREATE PROCEDURE sp_updateNifCliente
@name NVARCHAR(100) = 'Consumidor Final',
@newNif INT = 999999999
AS BEGIN
	DECLARE @pessoaID INT
	/*V� se o nome introduzido existe na tabela Pessoa*/
	SELECT @pessoaID = pessoaID FROM dbo.Pessoa WHERE nome = @name
	/*Se n�o existe n�o faz nada*/
	IF @pessoaID IS NULL
		BEGIN
			DECLARE @errorMessage NVARCHAR(50)
			SET @errorMessage = CONCAT('O nome ',@name,' n�o existe na base de dados')
			RAISERROR(@errorMessage,16,1)
		END
	ELSE
		BEGIN
			IF (@newNif IS NOT NULL)
				BEGIN TRAN
				UPDATE dbo.Cliente SET nif = @newNif WHERE pessoaID = @pessoaID;
				COMMIT TRAN
		END
END
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AEnima
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cliente
    {
        public Cliente()
        {
            this.Aluguers = new HashSet<Aluguer>();
        }
    
        public int pessoaID { get; set; }
        public Nullable<int> nif { get; set; }
        public string morada { get; set; }
    
        public virtual ICollection<Aluguer> Aluguers { get; set; }
        public virtual Pessoa Pessoa { get; set; }
    }
}

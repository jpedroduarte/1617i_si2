﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima
{
    class Utils
    {

        public static System.DateTime getDate(String dateName)
        {
            System.DateTime aux = new DateTime();
            bool exit = false;
            do
            {
                Console.WriteLine("Insert " + dateName + " Date format:dd/mm/yyyy");
                try
                {
                    aux = Convert.ToDateTime(Console.ReadLine());
                    exit = true;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid date");
                }
            } while (!exit);

            return aux;
        }

        public static Nullable<System.DateTime> getDateNullable(String dateName)
        {
            Nullable<System.DateTime> aux = null;
            bool exit = false;
            do
            {
                Console.WriteLine("Insert " + dateName + " Date format:dd/mm/yyyy");
                try
                {
                    String console = Console.ReadLine();

                    if (console.Equals("null"))  // || console.Equals(""))
                        return aux;
                    else
                    aux = Convert.ToDateTime(console);
                    exit = true;
                }
                catch (FormatException)
                {

                    Console.WriteLine("Invalid date");
                }
            } while (!exit);
            
            return aux;
        }

        public static int getInt(String param)
        {
            int aux = 0;
            bool exit = false;
            do
            {
                Console.WriteLine("Insert " + param + "Numeric");
                try
                {
                    String console = Console.ReadLine();
                    aux = Convert.ToInt32(console);
                    exit = true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid number");
                }
            } while (!exit);

            return aux;
        }

        public static Nullable<int> getIntNullable(String param)
        {
            Nullable<int> aux = null;
            bool exit = false;
            do
            {
                Console.WriteLine("Insert " + param + "Numeric");
                try
                {
                    String console = Console.ReadLine();

                    if (console.Equals("null"))  // || console.Equals(""))
                        return aux;
                    else
                    aux = Convert.ToInt32(console);
                    exit = true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid number");
                }
            } while (!exit);
            
            return aux;
        }

        public static String getString(String param)
        {
            Console.WriteLine("Insert " + param + "String");
            String console = Console.ReadLine();
            if (console.Equals("null"))
                return null;
            else
                return console;
        }
    }
}

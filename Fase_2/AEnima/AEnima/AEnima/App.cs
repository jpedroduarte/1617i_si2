﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Data;


namespace AEnima
{
    class App
    {
        
        private enum Option
        {
            Unknown = -1,
            Exit,
            InsertPromo,
            RemovePromo,
            UpdatePromo,
            InsertAluguerWithCompleteData,
            RemoveAluguerUsingExisitingClient,
            RemoveAluguer,
            UpdateValorPreco,
            UpdateFraccaoPreco,
            ListFreeEquipments,
            ListEquipmentsWithoutAluguerInLastWeek
        }
        private static App __instance;
        private App()
        {
            __dbMethods = new Dictionary<Option, DBMethod>();
            __dbMethods.Add(Option.InsertPromo, Commands.InsertPromo.Execute);
            __dbMethods.Add(Option.RemovePromo, Commands.RemovePromo.Execute);
            __dbMethods.Add(Option.UpdatePromo, Commands.RemovePromo.Execute);
            __dbMethods.Add(Option.InsertAluguerWithCompleteData, Commands.InsertAluguerWithCompleteData.Execute);
            __dbMethods.Add(Option.RemoveAluguerUsingExisitingClient, Commands.InsertAluguerUsingExisitingClient.Execute);
            __dbMethods.Add(Option.RemoveAluguer, Commands.RemoveAluguer.Execute);
            __dbMethods.Add(Option.UpdateValorPreco, Commands.UpdateValorPreco.Execute);
            //__dbMethods.Add(Option.UpdateFraccaoPreco, UpdateFraccaoPreco);???
            __dbMethods.Add(Option.ListFreeEquipments, Commands.ListFreeEquipments.Execute);
            __dbMethods.Add(Option.ListEquipmentsWithoutAluguerInLastWeek, Commands.ListEquipmentsWithoutAluguerInLastWeek.Execute);
        }

        public static App Instance
        {
            get
            {
                if (__instance == null)
                    __instance = new App();
                return __instance;
            }
            private set { }
        }

        private Option DisplayMenu()
        {
            Option option = Option.Unknown;
            try
            {
                Console.WriteLine("List of Commands");
                Console.WriteLine();
                Console.WriteLine("1. InsertPromo");
                Console.WriteLine("2. RemovePromo");
                Console.WriteLine("3. UpdatePromo");
                Console.WriteLine("4. InsertAluguerWithCompleteData");
                Console.WriteLine("5. RemoveAluguerUsingExisitingClient");
                Console.WriteLine("6. RemoveAluguer");
                Console.WriteLine("7. UpdateValorPreco");
                Console.WriteLine("8. UpdateFraccaoPreco");
                Console.WriteLine("9. ListFreeEquipments");
                Console.WriteLine("10. ListEquipmentsWithoutAluguerInLastWeek");
                Console.WriteLine("0. Exit");
                Console.WriteLine();
                Console.WriteLine("Insert a command number:");
                var result = Console.ReadLine();
                option = (Option)Enum.Parse(typeof(Option), result);
            }
            catch (ArgumentException)
            {
                //nothing to do. User press select no option and press enter.
            }

            return option;

        }
        private void Login()
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
            }

        }
        private delegate void DBMethod();
        private System.Collections.Generic.Dictionary<Option, DBMethod> __dbMethods;
        public string ConnectionString
        {
            get;
            set;
        }

        public void Run()
        {
            Login();
            Option userInput = Option.Unknown;
            do
            {
                Console.Clear();
                userInput = DisplayMenu();
                Console.Clear();
                try
                {
                    __dbMethods[userInput]();
                    Console.ReadKey();
                }
                catch (KeyNotFoundException)
                {
                    //Nothing to do. The option was not a valid one. Read another.
                }

            } while (userInput != Option.Exit);
        }

        
    }
}


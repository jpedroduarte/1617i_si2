﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AEnima;
using System.Collections;

namespace AEnima
{
    class Program
    {
        public static void Main(string[] args)
        {
            Credentials cr = getCredentials();
            //user: User1
            //pw: user1pwd
            Dictionary<string, string> map = new Dictionary<string, string>();
            map.Add("Server", "localhost");
            map.Add("User Id", cr.Username);
            map.Add("Database", "AEnima");
            map.Add("Password", cr.Password);
            //map.Add("Max Pool Size", "10");
            App.Instance.ConnectionString = createStringConn(map);

            App.Instance.Run();
        }

        private static String createStringConn(Dictionary<string, string> d)
        {
            String ret = "";
            foreach (KeyValuePair<string, string> curr in d)
            {
                ret += curr.Key + "=" + curr.Value + ";";
            }
            return ret;
        }

        private static string getConString(string server, string database, string user, string password)
        {
            return "Server=" + server + ";Database=" + database + ";User Id=" + user + ";Password=" + password + ";";
        }

        public static Credentials getCredentials()
        {
            Console.Write("Enter your username: ");
            string username = Console.ReadLine();
            string password = "";
            Console.Write("Enter your password: ");

            ConsoleKeyInfo key;

            do
            {
                key = Console.ReadKey(true);

                // Backspace Should Not Work
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    password += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && password.Length > 0)
                    {
                        password = password.Substring(0, (password.Length - 1));
                        Console.Write("\b \b");
                    }
                }
            }
            while (key.Key != ConsoleKey.Enter);

            return new Credentials(username, password);
        }
    }

    class Credentials
    {

        public string Username
        {
            get;
            private set;
        }
        public string Password
        {
            get;
            private set;
        }
        public Credentials(string username, string password)
        {
            Username = username;
            Password = password;
        }
    };
    
}

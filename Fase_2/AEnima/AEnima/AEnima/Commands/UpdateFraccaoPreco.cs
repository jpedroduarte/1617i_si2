﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima.Commands
{
    class UpdateFraccaoPreco
    {
        //TODO
        public static void Execute()
        {
            //string nomeTipo, int fracao, int valor
            /*
             * Get Params
             */
            Preco p = new Preco();
            p.nomeTipo = Utils.getString("nomeTipo");
            p.fraccao = Utils.getInt("fraccao");
            p.valor = Utils.getInt("valor");

            //dif
            const String thisMenu = "Update preco";
            Console.Clear();
            Console.WriteLine(thisMenu + " Menu");
            Console.WriteLine();
            Console.WriteLine("1. Connected objects");
            Console.WriteLine("2. Entity Framework");
            bool exit;
            do
            {
                exit = true;
                Console.WriteLine();
                Console.WriteLine("Insert a command number:");
                String result = Console.ReadLine();
                switch (result)
                {
                    case "1"://dif
                        //todo
                        break;
                    case "2"://dif
                        //todo
                        Console.WriteLine("Command executed. Press any key.");
                        break;
                    default:
                        Console.WriteLine("Insert a valid number.");
                        exit = false;
                        break;
                }
            } while (!exit);
        }

        private static void execEntityFrameWork(string nomeTipo, int fracao, int valor)
        {
            using (var ctx = new AEnimaEntities())
            {
                //dif
                int result = ctx.sp_updateValorPreco(
                    nomeTipo,
                    fracao,
                    valor
                    );

                ctx.SaveChanges();
            }
        }
    }
}

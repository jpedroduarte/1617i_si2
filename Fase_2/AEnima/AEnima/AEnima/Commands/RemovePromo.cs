﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima.Commands
{
    class RemovePromo
    {
        public static void Execute()
        {
            
            /*
             * Get Params
             */
            Promocao p = new Promocao();
            p.promocaoID = Utils.getInt("promocaoID");

            const String thisMenu = "Remove Promotion"; //dif
            Console.Clear();
            Console.WriteLine(thisMenu + " Menu");
            Console.WriteLine();
            Console.WriteLine("1. Connected objects");
            Console.WriteLine("2. Entity Framework");
            bool exit;
            do
            {
                exit = true;
                Console.WriteLine();
                Console.WriteLine("Insert a command number:");
                String result = Console.ReadLine();
                switch (result)
                {
                    case "1"://dif
                        ADOPromocao.remPromocao(p.promocaoID);
                        break;
                    case "2"://dif
                        execEntityFrameWork(p.promocaoID);
                        Console.WriteLine("Command executed. Press any key.");
                        break;
                    default:
                        Console.WriteLine("Insert a valid number.");
                        exit = false;
                        break;
                }
            } while (!exit);
        }

        private static void execEntityFrameWork(int promocaoID)
        {
            using (var ctx = new AEnimaEntities())
            {
                //dif
                int result = ctx.sp_removePromocao(promocaoID);
                
                ctx.SaveChanges();
            }
        }

    }
}

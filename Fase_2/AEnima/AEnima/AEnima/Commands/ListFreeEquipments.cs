﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima.Commands
{
    class ListFreeEquipments
    {
        public static void Execute()
        {
            //DateTime inicio, DateTime fim, string tipo
            /*
             * Get Params
             */
            DateTime dataHora = Utils.getDate("Data Inicio");
            DateTime dataFim = Utils.getDate("Data Fim");
            String nomeTipo = Utils.getString("Tipo Equipamento");

            //dif
            const String thisMenu = "List Free Equipments in timestamp";
            Console.Clear();
            Console.WriteLine(thisMenu + " Menu");
            Console.WriteLine();
            Console.WriteLine("1. Connected objects");
            Console.WriteLine("2. Entity Framework");
            bool exit;
            do
            {
                exit = true;
                Console.WriteLine();
                Console.WriteLine("Insert a command number:");
                String result = Console.ReadLine();
                switch (result)
                {
                    case "1"://dif
                        ADOEquipamento.listaEquipamentoLivreTempoTipo(
                            dataHora,
                            dataFim,
                            nomeTipo);
                        break;
                    case "2"://dif
                        execEntityFrameWork(
                           dataHora,
                            dataFim,
                            nomeTipo);
                        Console.WriteLine("Command executed. Press any key.");
                        break;
                    default:
                        Console.WriteLine("Insert a valid number.");
                        exit = false;
                        break;
                }
            } while (!exit);
        }

        private static void execEntityFrameWork(DateTime inicio, DateTime fim, string tipo)
        {
            using (var ctx = new AEnimaEntities())
            {
                //dif

                var result= ctx.equip_livres_tempo_tipo(inicio, fim, tipo);
                Console.WriteLine("id | nomeTipo | disponibilidade");
                foreach (var item in result)
                {
                    Console.WriteLine("{0} | {1} | {2}", item.equipamentoID, item.nomeTipo, item.disponibilidade);
                }
                ctx.SaveChanges();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima.Commands
{
    class UpdatePromo
    {
        public static void Execute()
        {

            /*
             * Get Params
             */
            Promocao p = new Promocao();
            p.promocaoID = Utils.getInt("promocaoID");
            p.dataInicio = Utils.getDate("dataInicio");
            p.dataFim = Utils.getDate("dataFim");
            p.descontoPrecoPorCento = Utils.getIntNullable("descontoPrecoPorCento");
            p.descontoFraccao = Utils.getIntNullable("descontoFraccao");
            p.descricao = Utils.getString("descricao");

            const String thisMenu = "Update Promotion"; //dif
            Console.Clear();
            Console.WriteLine(thisMenu + " Menu");
            Console.WriteLine();
            Console.WriteLine("1. Connected objects");
            Console.WriteLine("2. Entity Framework");
            bool exit;
            do
            {
                exit = true;
                Console.WriteLine();
                Console.WriteLine("Insert a command number:");
                String result = Console.ReadLine();
                switch (result)
                {
                    case "1"://dif
                        ADOPromocao.updPromocao(p.promocaoID, p.dataInicio, p.dataFim, p.descontoPrecoPorCento, p.descontoFraccao, p.descricao);
                        break;
                    case "2"://dif
                        execEntityFrameWork(p.promocaoID, p.dataInicio, p.dataFim, p.descontoPrecoPorCento, p.descontoFraccao, p.descricao);
                        Console.WriteLine("Command executed. Press any key.");
                        break;
                    default:
                        Console.WriteLine("Insert a valid number.");
                        exit = false;
                        break;
                }
            } while (!exit);
        }

        private static void execEntityFrameWork(int id, DateTime inicio, DateTime fim, int? descontoPreco = null,
            int? descontoFracao = null, string descricao = null)
        {
            using (var ctx = new AEnimaEntities())
            {
                //dif
                int result = ctx.sp_updatePromocao(
                    id, 
                    inicio,
                    fim,
                    descontoPreco,
                    descontoFracao,
                    descricao);

                ctx.SaveChanges();
            }
        }
    }
}

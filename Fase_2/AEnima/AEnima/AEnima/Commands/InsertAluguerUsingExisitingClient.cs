﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima.Commands
{
    class InsertAluguerUsingExisitingClient
    {
        public static void Execute()
        {
            //int cliID, int equipID, int codEmp
            /*
             * Get Params
             */
            Aluguer a = new Aluguer();
            a.Cliente.pessoaID = Utils.getInt("clienteID");
            a.equipamentoID = Utils.getInt("equipamentoID");
            a.codigoEmpregado = Utils.getInt("codigoEmpregado");

            //dif
            const String thisMenu = "Insert Aluguer With Existing Client";
            Console.Clear();
            Console.WriteLine(thisMenu + " Menu");
            Console.WriteLine();
            Console.WriteLine("1. Connected objects");
            Console.WriteLine("2. Entity Framework");
            bool exit;
            do
            {
                exit = true;
                Console.WriteLine();
                Console.WriteLine("Insert a command number:");
                String result = Console.ReadLine();
                switch (result)
                {
                    case "1"://dif
                        ADOAluguer.insAluguer(
                            a.Cliente.pessoaID,
                            a.equipamentoID,
                            a.codigoEmpregado);
                        break;
                    case "2"://dif
                        execEntityFrameWork(
                            a.Cliente.pessoaID,
                            a.equipamentoID,
                            a.codigoEmpregado);
                        Console.WriteLine("Command executed. Press any key.");
                        break;
                    default:
                        Console.WriteLine("Insert a valid number.");
                        exit = false;
                        break;
                }
            } while (!exit);
        }

        private static void execEntityFrameWork(int cliID, int equipID, int codEmp)
        {
            using (var ctx = new AEnimaEntities())
            {
                //dif
                int result = ctx.sp_aluguer_clienteRegistado(
                    cliID,
                    equipID,
                    codEmp);

                ctx.SaveChanges();
            }
        }
    }
}

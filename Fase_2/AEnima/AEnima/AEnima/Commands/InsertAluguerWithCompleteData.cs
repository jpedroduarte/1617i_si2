﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima.Commands
{
    class InsertAluguerWithCompleteData
    {
        public static void Execute()
        {
            //string nome, int nif, string morada, int equipID, int codEmp
            /*
             * Get Params
             */
            Aluguer a = new Aluguer();
            
            a.Cliente.Pessoa.nome = Utils.getString("nome");
            a.Cliente.nif = Utils.getIntNullable("nif");
            a.Cliente.morada = Utils.getString("morada");
            a.equipamentoID = Utils.getInt("equipamentoID");
            a.codigoEmpregado = Utils.getInt("codigoEmpregado");
            

            const String thisMenu = "Insert Aluguer With New Client"; //dif
            Console.Clear();
            Console.WriteLine(thisMenu + " Menu");
            Console.WriteLine();
            Console.WriteLine("1. Connected objects");
            Console.WriteLine("2. Entity Framework");
            bool exit;
            do
            {
                exit = true;
                Console.WriteLine();
                Console.WriteLine("Insert a command number:");
                String result = Console.ReadLine();
                switch (result)
                {
                    case "1"://dif
                        ADOAluguer.insAluguerNovoCliente(
                            a.Cliente.Pessoa.nome,
                            a.Cliente.nif,
                            a.Cliente.morada,
                            a.equipamentoID,
                            a.codigoEmpregado);
                        break;
                    case "2"://dif
                        execEntityFrameWork(
                            a.Cliente.Pessoa.nome,
                            a.Cliente.nif,
                            a.Cliente.morada,
                            a.equipamentoID,
                            a.codigoEmpregado);
                        Console.WriteLine("Command executed. Press any key.");
                        break;
                    default:
                        Console.WriteLine("Insert a valid number.");
                        exit = false;
                        break;
                }
            } while (!exit);
        }

        private static void execEntityFrameWork(string nome, int? nif, string morada, int equipID, int codEmp)
        {
            using (var ctx = new AEnimaEntities())
            {
                //dif
                int result = ctx.sp_aluguer_clienteNaoRegistado(
                    nome,
                    nif,
                    morada,
                    equipID,
                    codEmp);

                ctx.SaveChanges();
            }
        }
    }
}

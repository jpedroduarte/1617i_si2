﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima.Commands
{
    class RemoveAluguer
    {
        public static void Execute()
        {
            //int nSerie
            /*
             * Get Params
             */
            Aluguer a = new Aluguer();
            a.numeroSerie = Utils.getInt("numeroSerie");

            //dif
            const String thisMenu = "Remove Aluguer";
            Console.Clear();
            Console.WriteLine(thisMenu + " Menu");
            Console.WriteLine();
            Console.WriteLine("1. Connected objects");
            Console.WriteLine("2. Entity Framework");
            bool exit;
            do
            {
                exit = true;
                Console.WriteLine();
                Console.WriteLine("Insert a command number:");
                String result = Console.ReadLine();
                switch (result)
                {
                    case "1"://dif
                        ADOAluguer.remAluguer(
                            a.numeroSerie);
                        break;
                    case "2"://dif
                        execEntityFrameWork(
                            a.numeroSerie);
                        Console.WriteLine("Command executed. Press any key.");
                        break;
                    default:
                        Console.WriteLine("Insert a valid number.");
                        exit = false;
                        break;
                }
            } while (!exit);
        }

        private static void execEntityFrameWork(int nSerie)
        {
            using (var ctx = new AEnimaEntities())
            {
                //dif
                int result = ctx.sp_deleteAluguer(
                    nSerie);

                ctx.SaveChanges();
            }
        }
    }
}

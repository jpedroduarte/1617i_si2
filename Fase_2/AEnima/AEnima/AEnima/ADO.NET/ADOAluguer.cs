﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima
{
    class ADOAluguer
    {

        public static void insAluguerNovoCliente(string nome, int? nif, string morada, int equipID, int codEmp)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramNome = new SqlParameter("@name", SqlDbType.NVarChar, 100);
                            cmd.Parameters.Add(paramNome);
                            paramNome.Value = nome;

                            SqlParameter paramNIF = new SqlParameter("@nif", SqlDbType.Int);
                            cmd.Parameters.Add(paramNIF);
                            paramNIF.Value = nif;

                            SqlParameter paramMorada = new SqlParameter("@morada", SqlDbType.NVarChar, 300);
                            cmd.Parameters.Add(paramMorada);
                            paramMorada.Value = morada;

                            SqlParameter paramEquipID = new SqlParameter("@equipamentoID", SqlDbType.Int);
                            cmd.Parameters.Add(paramEquipID);
                            paramEquipID.Value = equipID;

                            SqlParameter paramCodEmp = new SqlParameter("@codigoEmpregado", SqlDbType.Int);
                            cmd.Parameters.Add(paramCodEmp);
                            paramCodEmp.Value = codEmp;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_aluguer_clienteNaoRegistado";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Equipamento com o id {0} alugado, pelo novo cliente {1}, " +
                                    "efetuado pelo empregado com o id {2}.", equipID, nome, codEmp);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void insAluguer(int cliID, int equipID, int codEmp)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramCliID = new SqlParameter("@clienteID", SqlDbType.Int);
                            cmd.Parameters.Add(paramCliID);
                            paramCliID.Value = cliID;

                            SqlParameter paramEquipID = new SqlParameter("@equipamentoID", SqlDbType.Int);
                            cmd.Parameters.Add(paramEquipID);
                            paramEquipID.Value = equipID;

                            SqlParameter paramCodEmp = new SqlParameter("@codigoEmpregado", SqlDbType.Int);
                            cmd.Parameters.Add(paramCodEmp);
                            paramCodEmp.Value = codEmp;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_aluguer_clienteRegistado";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Equipamento com o id {0} alugado, pelo cliente com id {1}, " +
                                    "efetuado pelo empregado com o id {2}.", equipID, cliID, codEmp);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void remAluguer(int nSerie)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramNSerie = new SqlParameter("@numerSerie", SqlDbType.Int);
                            cmd.Parameters.Add(paramNSerie);
                            paramNSerie.Value = nSerie;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_deleteAluguer";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Aluguer com o número de série {0} apagado!", nSerie);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void fecharAluguer(int nSerie)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramNSerie = new SqlParameter("@numeroSerie", SqlDbType.Int);
                            cmd.Parameters.Add(paramNSerie);
                            paramNSerie.Value = nSerie;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_entrega_aluguer";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Aluguer com o número de série {0} entregue!", nSerie);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

    }
}

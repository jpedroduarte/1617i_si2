﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima
{
    class ADOEquipamento
    {
        private static SqlConnection conn = new SqlConnection();

        public static void remEquipamento(int id)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramID = new SqlParameter("@id", SqlDbType.Int);
                            cmd.Parameters.Add(paramID);
                            paramID.Value = id;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_deleteEquipamento";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Equipamento com o id {0} apagado!", id);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void insEquipamento(string nome, string descricao = null)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramNome = new SqlParameter("@nomeTipo", SqlDbType.NVarChar, 50);
                            cmd.Parameters.Add(paramNome);
                            paramNome.Value = nome;

                            SqlParameter paramDesc = new SqlParameter("@descricao", SqlDbType.NVarChar, 300);
                            cmd.Parameters.Add(paramDesc);
                            paramDesc.Value = descricao;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_insertEquipamento";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Equipamento {0} com a descrição {1} foi inserido!", nome, descricao);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void updEquipamento(int id, string nome, string descricao = null)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramID = new SqlParameter("@id", SqlDbType.Int);
                            cmd.Parameters.Add(paramID);
                            paramID.Value = id;

                            SqlParameter paramNome = new SqlParameter("@nomeTipo", SqlDbType.NVarChar, 50);
                            cmd.Parameters.Add(paramNome);
                            paramNome.Value = nome;

                            SqlParameter paramDescricao = new SqlParameter("@descricao", SqlDbType.NVarChar, 50);
                            cmd.Parameters.Add(paramDescricao);
                            paramDescricao.Value = descricao;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_updateEquipamento";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Equipamento com o id {0} é do tipo {1} com a descrição {2}!", id, nome, descricao);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void listaEquipamentoLivreTempoTipo(DateTime inicio, DateTime fim, string tipo)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = new SqlCommand("SELECT * FROM dbo.equip_livres_tempo_tipo(@dataInicio, @dataFim, @tipo)", conn))
                        {
                            SqlParameter paramInicio = new SqlParameter("@dataInicio", SqlDbType.Date);
                            cmd.Parameters.Add(paramInicio);
                            paramInicio.Value = inicio;

                            SqlParameter paramFim = new SqlParameter("@dataFim", SqlDbType.Date);
                            cmd.Parameters.Add(paramFim);
                            paramFim.Value = fim;

                            SqlParameter paramTipo = new SqlParameter("@tipo", SqlDbType.NVarChar, 50);
                            cmd.Parameters.Add(paramTipo);
                            paramTipo.Value = tipo;

                            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                            DataTable dataTable = new DataTable();
                            dataAdapter.Fill(dataTable);

                            Console.WriteLine(" -=# Equipamentos do tipo 'Barco' disponíveis entre " + inicio.Day + "-"
                                + inicio.Month + "-" + inicio.Year + " e " + fim.Day + "-" + fim.Month + "-"
                                + fim.Year + " #=-\n ");
                            Console.WriteLine("IDEquip\tTipo\tDescrição\tDisponibilidade");

                            foreach (DataRow row in dataTable.Rows)
                            {
                                Console.WriteLine(row[0].ToString() + "\t"
                                                + row[1].ToString() + "\t"
                                                + row[2].ToString() + "\t"
                                                + row[3]);
                            }

                            Console.Read();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void listaEquipamentoSemAluguerUltimaSemana()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = new SqlCommand("SELECT * FROM dbo.view_equipamento_within_last_week", conn))
                        {
                            SqlDataReader dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                            if (dataReader.HasRows)
                            {
                                Console.WriteLine(" -=# Equipamentos Sem Aluguer Na Última Semana #=- ");
                                Console.WriteLine("| ID\t| Descrição\t| Cliente\t| Empregado\t| Início\t| Fim\t| Duração\t| Valor\t|");

                                string desc = "";
                                string cli = "";
                                string emp = "";
                                DateTime? fim = null;
                                int? duracao = null;
                                decimal? valor = null;

                                while (dataReader.Read())
                                {
                                    if (!dataReader.IsDBNull(1))
                                        desc = dataReader.GetString(1);
                                    if (!dataReader.IsDBNull(2))
                                        cli = dataReader.GetString(2);
                                    if (!dataReader.IsDBNull(3))
                                        emp = dataReader.GetString(3);
                                    if (!dataReader.IsDBNull(5))
                                        fim = dataReader.GetDateTime(5);
                                    if (!dataReader.IsDBNull(6))
                                        duracao = dataReader.GetInt32(6);
                                    if (!dataReader.IsDBNull(7))
                                        valor = dataReader.GetDecimal(7);
                                    Console.WriteLine("| " + dataReader.GetInt32(0) + "\t| " +
                                        desc + "\t| " +
                                        cli + "\t| " +
                                        emp + "\t| " +
                                        dataReader.GetDateTime(4) + "\t| " +
                                        fim + "\t| " +
                                        duracao + "\t|" +
                                        valor + "\t|");
                                }
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima
{
    class ADOPromocao
    {

        public static void insPromocao(DateTime inicio, DateTime fim, int? descontoPreco = null,
            int? descontoFracao = null, string descricao = null)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                try
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramInicio = new SqlParameter("@dataInicio", SqlDbType.Date);
                            cmd.Parameters.Add(paramInicio);
                            paramInicio.Value = inicio;

                            SqlParameter paramFim = new SqlParameter("@dataFim", SqlDbType.Date);
                            cmd.Parameters.Add(paramFim);
                            paramFim.Value = fim;

                            SqlParameter paramDescPreco = new SqlParameter("@descontoPrecoPorCento", SqlDbType.Int);
                            cmd.Parameters.Add(paramDescPreco);
                            paramDescPreco.Value = descontoPreco;

                            SqlParameter paramDescFrac = new SqlParameter("@descontoFraccao", SqlDbType.Int);
                            cmd.Parameters.Add(paramDescFrac);
                            paramDescFrac.Value = descontoFracao;

                            SqlParameter paramDesc = new SqlParameter("@descricao", SqlDbType.NVarChar, 300);
                            cmd.Parameters.Add(paramDesc);
                            paramDesc.Value = descricao;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_insertPromocao";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Promoção com a descrição {0}, entre {1} e {2}, foi inserida!", descricao, inicio, fim);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void remPromocao(int id)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                try
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramID = new SqlParameter("@id", SqlDbType.Int);
                            cmd.Parameters.Add(paramID);
                            paramID.Value = id;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_removePromocao";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Promocao com o id {0} apagada!", id);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void updPromocao(int id, DateTime inicio, DateTime fim, int? descontoPreco = null,
            int? descontoFracao = null, string descricao = null)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                try
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramID = new SqlParameter("@id", SqlDbType.Int);
                            cmd.Parameters.Add(paramID);
                            paramID.Value = id;

                            SqlParameter paramInicio = new SqlParameter("@dataInicio", SqlDbType.Date);
                            cmd.Parameters.Add(paramInicio);
                            paramInicio.Value = inicio;

                            SqlParameter paramFim = new SqlParameter("@dataFim", SqlDbType.Date);
                            cmd.Parameters.Add(paramFim);
                            paramFim.Value = fim;

                            SqlParameter paramDescPreco = new SqlParameter("@descontoPrecoPorCento", SqlDbType.Int);
                            cmd.Parameters.Add(paramDescPreco);
                            paramDescPreco.Value = descontoPreco;

                            SqlParameter paramDescFrac = new SqlParameter("@descontoFraccao", SqlDbType.Int);
                            cmd.Parameters.Add(paramDescFrac);
                            paramDescFrac.Value = descontoFracao;

                            SqlParameter paramDesc = new SqlParameter("@descricao", SqlDbType.NVarChar, 300);
                            cmd.Parameters.Add(paramDesc);
                            paramDesc.Value = descricao;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_updatePromocao";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Promoção com a descrição {0}, entre {1} e {2}, foi atualizada!", descricao, inicio, fim);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima
{
    class ADOPreco
    {
        private static SqlConnection conn = new SqlConnection();

        public static void updPreco(string nomeTipo, int fracao, int valor)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                try
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramNomeTipo = new SqlParameter("@nomeTipo", SqlDbType.NVarChar, 50);
                            cmd.Parameters.Add(paramNomeTipo);
                            paramNomeTipo.Value = nomeTipo;

                            SqlParameter paramFracao = new SqlParameter("@fraccao", SqlDbType.Int);
                            cmd.Parameters.Add(paramFracao);
                            paramFracao.Value = fracao;

                            SqlParameter paramValor = new SqlParameter("@valor", SqlDbType.Int);
                            cmd.Parameters.Add(paramValor);
                            paramValor.Value = valor;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_updateValorPreco";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Preçário alterado!");
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }
    }
}

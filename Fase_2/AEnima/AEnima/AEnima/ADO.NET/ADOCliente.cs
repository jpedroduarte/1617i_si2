﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEnima
{
    static class ADOCliente
    {
        private static SqlConnection conn = new SqlConnection();

        public static void remCliente(int id)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramID = new SqlParameter("@pid", SqlDbType.Int);
                            cmd.Parameters.Add(paramID);
                            paramID.Value = id;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_deleteCliente";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Cliente com o id {0} apagado!", id);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void insCliente()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_insertCliente";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Cliente anónimo inserido!");
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void insCliente(string nome, int nif, string morada)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramNome = new SqlParameter("@name", SqlDbType.NVarChar, 100);
                            cmd.Parameters.Add(paramNome);
                            paramNome.Value = nome;

                            SqlParameter paramNIF = new SqlParameter("@nif", SqlDbType.Int);
                            cmd.Parameters.Add(paramNIF);
                            paramNIF.Value = nif;

                            SqlParameter paramMorada = new SqlParameter("@morada", SqlDbType.NVarChar, 300);
                            cmd.Parameters.Add(paramMorada);
                            paramMorada.Value = morada;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_insertCliente";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Cliente inserido!");
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void updNomeCliente(int id, string nome)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramID = new SqlParameter("@pid", SqlDbType.Int);
                            cmd.Parameters.Add(paramID);
                            paramID.Value = id;

                            SqlParameter paramNome = new SqlParameter("@newName", SqlDbType.NVarChar, 100);
                            cmd.Parameters.Add(paramNome);
                            paramNome.Value = nome;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_updateNomeCliente";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Nome ({0}) atualizado no cliente com o id {1}!", nome, id);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void updNIFCliente(int id, int nif)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramID = new SqlParameter("@pid", SqlDbType.Int);
                            cmd.Parameters.Add(paramID);
                            paramID.Value = id;

                            SqlParameter paramNIF = new SqlParameter("@newNif", SqlDbType.Int);
                            cmd.Parameters.Add(paramNIF);
                            paramNIF.Value = nif;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_updateNifCliente";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("NIF ({0}) atualizado no cliente com o id {1}!", nif, id);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }

        public static void updMoradaCliente(int id, string morada)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AEnimaCS"].ConnectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            SqlParameter paramID = new SqlParameter("@pid", SqlDbType.Int);
                            cmd.Parameters.Add(paramID);
                            paramID.Value = id;

                            SqlParameter paramMorada = new SqlParameter("@newMorada", SqlDbType.NVarChar, 300);
                            cmd.Parameters.Add(paramMorada);
                            paramMorada.Value = morada;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_updateMoradaCliente";

                            int row = cmd.ExecuteNonQuery();
                            if (row != 0)
                            {
                                Console.WriteLine("Morada ({0}) atualizado no cliente com o id {1}!", morada, id);
                                Console.Read();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO: " + ex.Message);
                    Console.Read();
                }
            }
        }
    }
}
